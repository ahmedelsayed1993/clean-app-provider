package com.aait.cleanappprovider.UI.Activities.Provider

import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.ClothesAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClothesServicesActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.edit){
            servicesPrices.clear()
            for (i in 0..clothesAdapter.getData().get(position).services_price?.size!!-1){
                if (clothesAdapter.getData().get(position).services_price?.get(i)?.price.equals(""))
                {
                   CommonUtil.makeToast(mContext,getString(R.string.enter_price))
                }else {
                    servicesPrices.add(
                        ClothesServiceModel(
                            clothesAdapter.getData().get(position).services_price?.get(
                                i
                            )?.price.toString(),
                            clothesAdapter.getData().get(position).services_price?.get(i)?.id.toString()
                        )
                    )
                }
            }
            Log.e("services", Gson().toJson(servicesPrices))
            Update(clothesServicesModels.get(position)?.id!!,1,Gson().toJson(servicesPrices))
        }else if (view.id == R.id.delete){
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.delete_dialog)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener { dialog?.dismiss() }
            confirm.setOnClickListener {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.deleteService(
                    mLanguagePrefManager.appLanguage, mSharedPrefManager.userData.id!!
                    , clothesServicesModels.get(position).id!!
                )?.enqueue(object : Callback<AboutAppResponse> {
                    override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(
                        call: Call<AboutAppResponse>,
                        response: Response<AboutAppResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.value.equals("1")) {
                                CommonUtil.makeToast(mContext, response.body()?.data!!)
                                dialog?.dismiss()
                                Services()
                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
            dialog?.show()
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_services
    lateinit var services: RecyclerView
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var lay: LinearLayout
    lateinit var no_Services: LinearLayout
    lateinit var add: Button
    lateinit var clothesAdapter: ClothesAdapter
    var clothesServicesModels = ArrayList<ClothesServicesModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var add_service:Button
    var servicesPrices = ArrayList<ClothesServiceModel>()

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        lay = findViewById(R.id.lay)
        no_Services = findViewById(R.id.no_services)
        add = findViewById(R.id.add)
        add_service = findViewById(R.id.add_service)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        title.text = mSharedPrefManager.userData.category_name
        services = findViewById(R.id.services)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        clothesAdapter = ClothesAdapter(mContext,clothesServicesModels,R.layout.recycler_services_clothes)
        clothesAdapter.setOnItemClickListener(this)
        services.layoutManager = linearLayoutManager
        services.adapter = clothesAdapter
        Services()
        add.setOnClickListener {
            val intent = Intent(this@ClothesServicesActivity, SubCategoryActivity::class.java)
            intent.putExtra("user", mSharedPrefManager.userData.id!!)
            intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
            intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
            startActivity(intent)
            this@ClothesServicesActivity.finish()
        }
        add_service.setOnClickListener {
            val intent = Intent(this@ClothesServicesActivity, SubCategoryActivity::class.java)
            intent.putExtra("user", mSharedPrefManager.userData.id!!)
            intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
            intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
            startActivity(intent)
            this@ClothesServicesActivity.finish()
        }

    }
    fun Services(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ClothesServices(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(
            object : Callback<ClothesServicesResponse> {
                override fun onFailure(call: Call<ClothesServicesResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<ClothesServicesResponse>,
                    response: Response<ClothesServicesResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                lay.visibility = View.GONE
                                no_Services.visibility =  View.VISIBLE

                            }else{
                                lay.visibility = View.VISIBLE
                                no_Services.visibility =  View.GONE
                                clothesAdapter.updateAll(response.body()?.data!!)
                            }
                        }
                    }
                }

            }
        )
    }
    fun Update(product:Int,services:Int,price:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.updatePrice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,product,services,price)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<AboutAppResponse>, response: Response<AboutAppResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        Services()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}