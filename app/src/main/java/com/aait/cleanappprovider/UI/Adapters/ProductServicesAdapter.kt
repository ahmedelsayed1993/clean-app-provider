package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.R




class ProductServicesAdapter (context: Context, data: MutableList<ProductModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProductModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        if (viewHolder.check.isChecked){

            productModel.selected = true
            // notifyDataSetChanged()
        }else{

            productModel.selected = false
            // notifyDataSetChanged()
        }
        viewHolder.name!!.setText(productModel?.name)
        viewHolder.servAdapter = ServAdapter(mcontext,viewHolder.serviceModels)
        viewHolder.services.layoutManager = GridLayoutManager(mcontext,productModel.services?.size!!)
        viewHolder.services.adapter = viewHolder.servAdapter
        viewHolder.servAdapter.updateAll(productModel?.services!!)
//       viewHolder.addServiceAdapter?.setData(data.get(position)?.services!!)
//        viewHolder.addServiceAdapter = AddServiceAdapter(mcontext,data.get(position)?.services!!)
//        viewHolder.services!!.layoutManager = LinearLayoutManager(mcontext,LinearLayoutManager.HORIZONTAL,false)
//        viewHolder.services!!.adapter = viewHolder.addServiceAdapter
//        viewHolder.addServiceAdapter!!.updateAll(data.get(position)?.services!!)

        viewHolder.check.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {


        internal lateinit var servAdapter:ServAdapter
        internal var serviceModels = ArrayList<ServiceModel>()


        internal var name=itemView.findViewById<TextView>(R.id.name)
       internal var services = itemView.findViewById<RecyclerView>(R.id.services)
        internal var check = itemView.findViewById<CheckBox>(R.id.check)




    }
}


