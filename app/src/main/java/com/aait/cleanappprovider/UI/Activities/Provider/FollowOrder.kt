package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.AboutAppResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FollowOrder:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_follow

    lateinit var one: ImageView
    lateinit var two: ImageView
    lateinit var three: ImageView
    lateinit var received: Button
    lateinit var work_underway: Button
    lateinit var back_to_main: Button
    lateinit var back: ImageView
    lateinit var title: TextView
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        received = findViewById(R.id.received)
        work_underway = findViewById(R.id.work_underway)
        back_to_main = findViewById(R.id.back_to_main)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        title.text = getString(R.string.follow_up_order)
        back.setOnClickListener { onBackPressed()
            finish()
        }
        back_to_main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        // accepted  1
        // received_order_client 2
        // received_order_delegate 3
        FollowOrder(null,null)
        received.setOnClickListener { FollowOrder("received_order_delegate",1) }
        work_underway.setOnClickListener { FollowOrder("work_underway",1) }



    }
    fun FollowOrder(status:String?,confirm:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.followOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
            id,status,confirm)?.enqueue(object : Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Log.e("response",response.body()?.data!!)
                        if (response.body()?.data.equals("received_order_client")){
                            received.visibility = View.VISIBLE
                            work_underway.visibility = View.GONE
                            back_to_main.visibility = View.GONE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.white_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.white_circle)

                        }else if (response.body()?.data.equals("received_order_delegate")){
                            received.visibility = View.GONE
                            work_underway.visibility = View.VISIBLE
                            back_to_main.visibility = View.GONE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.yellow_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.white_circle)

                        }else if (response.body()?.data.equals("work_underway")){
                            received.visibility = View.GONE
                            work_underway.visibility = View.GONE
                            back_to_main.visibility = View.VISIBLE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.yellow_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.yellow_circle)

                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

}