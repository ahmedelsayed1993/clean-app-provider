package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ClothesServiceModel
import com.aait.cleanappprovider.Models.ClothesServicesModel
import com.aait.cleanappprovider.Models.LocationServiceModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class ClothesServicesAdapter (context: Context, data: MutableList<ClothesServiceModel>) :
    ParentRecyclerAdapter<ClothesServiceModel>(context, data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_clothes_services, parent, false)
        return ViewHolder(itemView)
    }
    var android.widget.TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        viewHolder.name!!.setText(productModel?.service_name)
        viewHolder.price.setText(productModel.price)
        viewHolder.price.addTextChangedListener (object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                productModel?.price = charSequence.toString()



            }

            override fun afterTextChanged(editable: Editable) {}
        })











    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.service)
        internal var price = itemView.findViewById<EditText>(R.id.price)

//        internal fun ViewHolder(itemView: View) {
//
//            setClickableRootView(itemView)
////            addServiceAdapter = AddServiceAdapter(mcontext)
//            services.setLayoutManager(
//                LinearLayoutManager(
//                    mcontext,
//                    LinearLayoutManager.HORIZONTAL,
//                    false
//                )
//            )
//
//            services.adapter = addServiceAdapter
//        }



    }
}