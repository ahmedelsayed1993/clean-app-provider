package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.AboutAppResponse
import com.aait.cleanappprovider.Models.DelegateOrdersModel
import com.aait.cleanappprovider.Models.SendOrderDelegateResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.DelegateOrdersAdapter
import com.aait.cleanappprovider.UI.Adapters.NewOrdersAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDelegateDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_detegate_details
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var description:TextView
    lateinit var send:Button
    lateinit var orders:RecyclerView
    var id = 0
    var type = ""
    var order = 0
    var order_type = ""
    lateinit var linearLayoutManager: LinearLayoutManager
    var ordersModel = ArrayList<DelegateOrdersModel>()
    lateinit var delegateOrdersAdapter: DelegateOrdersAdapter

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        type = intent.getStringExtra("type")
        order = intent.getIntExtra("order",0)
        order_type = intent.getStringExtra("order_type")
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        description = findViewById(R.id.description)
        send = findViewById(R.id.send)
        orders = findViewById(R.id.orders)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        delegateOrdersAdapter = DelegateOrdersAdapter(mContext,ordersModel,R.layout.recycler_delegate_orders)
        orders.layoutManager = linearLayoutManager
        orders.adapter = delegateOrdersAdapter
        delegate()
        send.setOnClickListener {
            if (order_type.equals("normal")) {
                SendOrder()
            }else if (order_type.equals("abnormal")){
                SendOrder()
                FollowOrder("finished_order_provider",1)
            }
        }

    }

    fun FollowOrder(status:String?,confirm:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.followOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
            order,status,confirm)?.enqueue(object : Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Log.e("response",response.body()?.data!!)


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun delegate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.sendDelegate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
        ,id,order,type,null)?.enqueue(object : Callback<SendOrderDelegateResponse> {
            override fun onFailure(call: Call<SendOrderDelegateResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SendOrderDelegateResponse>,
                response: Response<SendOrderDelegateResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.text = response.body()?.data?.name
                        description.text = response.body()?.data?.description
                        delegateOrdersAdapter.updateAll(response.body()?.delegate_orders!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun SendOrder(){
       // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.sendDelegate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
            ,id,order,type,1)?.enqueue(object : Callback<SendOrderDelegateResponse> {
            override fun onFailure(call: Call<SendOrderDelegateResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SendOrderDelegateResponse>,
                response: Response<SendOrderDelegateResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@OrderDelegateDetailsActivity,DelegateDoneActivity::class.java))
                        this@OrderDelegateDetailsActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}