package com.aait.cleanappprovider.UI.Activities.Provider

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.InvoiceResponse
import com.aait.cleanappprovider.Models.ItemInvoiceModel
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.InvoiceAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InvoiceActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_invoice

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var items:RecyclerView
    lateinit var vat:TextView
    lateinit var vat_price:TextView
    lateinit var delivery_price:TextView
    lateinit var total:TextView
    var itemInvoiceModels = ArrayList<ItemInvoiceModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var invoiceAdapter: InvoiceAdapter
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        items = findViewById(R.id.items)
        vat = findViewById(R.id.vat)
        vat_price = findViewById(R.id.vat_price)
        delivery_price = findViewById(R.id.delivery_price)
        total = findViewById(R.id.total)
        title.text = getString(R.string.invoice)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        invoiceAdapter = InvoiceAdapter(mContext,itemInvoiceModels,R.layout.recycler_invoice)
        items.layoutManager = linearLayoutManager
        items.adapter = invoiceAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        Invoice()

    }

    fun Invoice(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getInvoice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(
            object : Callback<InvoiceResponse>{
                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {
                   hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            vat.text = response.body()?.tax+" % "
                            vat_price.text = response.body()?.total_tax +" "+ getString(R.string.rs)
                            total.text = response.body()?.total +" "+getString(R.string.rs)
                            delivery_price.text = response.body()?.delivery +" "+getString(R.string.rs)
                            invoiceAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}