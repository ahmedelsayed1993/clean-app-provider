package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Fragments.*

class SubscribeDelegateTapAdapter (private val context: Context,
fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            DelegateCurrentFragment()
        } else{
            DelegateFinishedOrders()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.the_current_orders)
        } else{
            context.getString(R.string.Previous_orders)
        }
    }
}
