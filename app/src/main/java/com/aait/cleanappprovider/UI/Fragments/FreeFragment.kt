package com.aait.cleanappprovider.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanappprovider.Base.BaseFragment
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.DelegateModel
import com.aait.cleanappprovider.Models.DelegatesResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Provider.OrderDelegateDetailsActivity
import com.aait.cleanappprovider.UI.Adapters.DelegatesAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FreeFragment :BaseFragment(), OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if(orderModels.get(position).confirm==0||orderModels.get(position).active==0){

        }else {
            val intent = Intent(activity, OrderDelegateDetailsActivity::class.java)
            intent.putExtra("id", orderModels.get(position).id)
            intent.putExtra("type", "free")
            intent.putExtra("order", id)
            intent.putExtra("order_type", type)
            startActivity(intent)
        }

    }

    override val layoutResource: Int
        get() = R.layout.fragment_delegates
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null
    lateinit var search_text:EditText
    lateinit var search:ImageView
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var orderModels = java.util.ArrayList<DelegateModel>()
    internal lateinit var orderAdapter: DelegatesAdapter
    companion object {
        fun newInstance(id:Int,type:String): FreeFragment {
            val args = Bundle()
            val fragment = FreeFragment()
            args.putInt("id",id)
            args.putString("type",type)
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var bundle:Bundle
    internal var id: Int = 0
    internal var type:String = ""
    override fun initializeComponents(view: View) {
        bundle= this.arguments!!
        id = bundle.getInt("id")
        type = bundle.getString("type").toString()
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        orderAdapter =  DelegatesAdapter(mContext!!,orderModels,R.layout.recycler_order_delegate)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = orderAdapter
        search_text = view.findViewById(R.id.search_text)
        search = view.findViewById(R.id.search)

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome(null) }
        getHome(null)
        search.setOnClickListener {
            if (search_text.text.toString().equals("")){

            }else{
                getHome(search_text.text.toString()!!)
            }
        }
    }
    fun getHome(search:String?){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.orderDelegats(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"free",search)?.enqueue(object :
            Callback<DelegatesResponse> {
            override fun onResponse(call: Call<DelegatesResponse>, response: Response<DelegatesResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                orderAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<DelegatesResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })
    }
}