package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.UserResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Provider.NewPasswordActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_forgot_pass

    lateinit var phone: EditText
    lateinit var confirm: Button


    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        confirm = findViewById(R.id.next)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))){
                return@setOnClickListener
            }else{
                ForgotPass()
            }
        }

    }
    fun ForgotPass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ForGot(phone.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type.equals("provider")) {
                            val intent =
                                Intent(this@ForgotPasswordActivity, NewPasswordActivity::class.java)
                            intent.putExtra("data", response.body()?.data)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,"هذا ليس حساب مقدم خدمة")
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })

    }
}