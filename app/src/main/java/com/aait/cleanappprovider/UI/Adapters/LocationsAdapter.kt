package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.LocationServiceModel
import com.aait.cleanappprovider.Models.ProductModel
import com.aait.cleanappprovider.Models.SubCategoryModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class LocationsAdapter (context: Context, data: MutableList<SubCategoryModel>, layoutId: Int) :
    ParentRecyclerAdapter<SubCategoryModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var android.widget.TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        viewHolder.cat!!.setText(productModel?.name)
        viewHolder.productsAdapter = LocationSerAdapter(mcontext,viewHolder.productModels,R.layout.recycler_cars,productModel.category_id!!)
        viewHolder.services.layoutManager = GridLayoutManager(mcontext,
            2)
        viewHolder.services.adapter = viewHolder.productsAdapter
        viewHolder.productsAdapter.updateAll(productModel.products!!)
       // viewHolder.services.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        lateinit var productsAdapter:LocationSerAdapter
        internal var productModels = ArrayList<ProductModel>()
        internal var cat=itemView.findViewById<TextView>(R.id.cat)
        internal var services = itemView.findViewById<RecyclerView>(R.id.services)


    }
}