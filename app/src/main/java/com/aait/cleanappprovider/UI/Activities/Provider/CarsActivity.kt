package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.CarServicesAdapter
import com.aait.cleanappprovider.UI.Adapters.CarsAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.recycler_cars.view.*
import kotlinx.android.synthetic.main.recycler_invoice.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarsActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {


    }

    override val layoutResource: Int
        get() = R.layout.activity_cars
    lateinit var size:TextView
    lateinit var sizes:RecyclerView
    lateinit var services:RecyclerView
    lateinit var confirm:Button
    lateinit var carsAdapter:CarsAdapter
    var productModels = ArrayList<ProductModel>()
    var servicesModels = ArrayList<ListModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var carServicesAdapter:CarServicesAdapter
    lateinit var title:TextView
    lateinit var back:ImageView
    var user = 0
    var category_id = ""
    var category_name = ""
    var subcategory = ""
    var cars = ArrayList<CarModel>()
    var subModels = ArrayList<ServicesCarModel>()
    override fun initializeComponents() {
        user = intent.getIntExtra("user" ,0)
        category_id = intent.getStringExtra("category_id")
        category_name = intent.getStringExtra("category_name")
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.visibility = View.VISIBLE
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = category_name
        size = findViewById(R.id.size)
        sizes = findViewById(R.id.sizes)
        services = findViewById(R.id.services)
        confirm = findViewById(R.id.confirm)
        gridLayoutManager = GridLayoutManager(mContext,2)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        carServicesAdapter = CarServicesAdapter(mContext,servicesModels,R.layout.recycler_service_cars)
        carsAdapter = CarsAdapter(mContext,productModels,R.layout.recycler_cars)
//        carsAdapter.setOnItemClickListener(this)
//        carServicesAdapter.setOnItemClickListener(this)
        sizes.layoutManager = gridLayoutManager
        sizes.adapter = carsAdapter
        services.layoutManager = linearLayoutManager
        services.adapter = carServicesAdapter
        getSubCategories(user,category_id.toInt())
        confirm.setOnClickListener {
            cars.clear()
            subModels.clear()
           carsAdapter.notifyDataSetChanged()
            carServicesAdapter.notifyDataSetChanged()
            for (i in 0..carsAdapter.getData().size - 1){
                if (!carsAdapter.getData().get(i).selected!!){
                    for (j in 0..cars.size-1){
                        if (cars.get(j).product!!.equals(carsAdapter.getData().get(i).id.toString())){
                           // cars.removeAt(j)
                            Log.e("cars",Gson().toJson(cars))
                        }
                    }

                }else{
                    if (!carsAdapter.getData().get(i).price.equals("")){
                        cars.add(
                            CarModel(
                                carsAdapter.getData().get(i).id.toString(),
                                carsAdapter.getData().get(i).price,
                                subcategory
                            )
                        )

                    }else {
                        CommonUtil.makeToast(mContext,getString(R.string.enter_prices))
                    }
                }
            }
            Log.e("cars",Gson().toJson(cars))
            for (i in 0..carServicesAdapter.getData().size - 1){
                if (carServicesAdapter.getData().get(i).checked!! !=1){
                    for (j in 0..subModels.size-1){
                        if (subModels.get(j).service_id!!.equals(carServicesAdapter.getData().get(i).id.toString())){
                            subModels.removeAt(j)
                        }
                    }
                }else{
                    if (!carServicesAdapter.getData().get(i).price.equals("")){
                        Log.e("servic",Gson().toJson(subModels))
                        subModels.add(
                            ServicesCarModel(
                                carServicesAdapter.getData().get(i).id.toString(),
                                carServicesAdapter.getData().get(i).price, subcategory
                            )
                        )


                    }else {
                        CommonUtil.makeToast(mContext,getString(R.string.enter_prices))
                    }

                }
            }
            Log.e("services",Gson().toJson(subModels))
            if (cars.isEmpty()){
                 CommonUtil.makeToast(mContext,getString(R.string.choose_cat))
            }
            else
            {
                if (subModels.isEmpty()) {
                    addProducts(null)
                }else {

                    if (cars.get(0).price.equals("")) {
                        CommonUtil.makeToast(mContext, getString(R.string.enter_price))
                    } else if (subModels.get(0).price.equals("")) {
                        CommonUtil.makeToast(mContext, getString(R.string.enter_price))
                    } else {
                        addProducts(Gson().toJson(subModels))
                    }
                }
            }
        }



    }
    fun getSubCategories(user_id:Int,cat:Int){
        showProgressDialog(mContext.getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.subCategory(mLanguagePrefManager.appLanguage,user_id,cat)?.enqueue(object :
            Callback<SubCategoryResponse> {
            override fun onFailure(call: Call<SubCategoryResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoryResponse>,
                response: Response<SubCategoryResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                       // servicesAdapter.updateAll(response.body()?.services!!)
                        subcategory = response.body()?.data?.get(0)?.id.toString()
                        size.text = response.body()?.data?.get(0)?.name
                        carsAdapter.updateAll(response.body()?.data?.get(0)?.products!!)
                        carServicesAdapter.updateAll(response.body()?.services!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun addProducts(services:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.addProduct(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData?.id!!,Gson().toJson(cars),services)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@CarsActivity,CarServicesActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        }

        )
    }
}