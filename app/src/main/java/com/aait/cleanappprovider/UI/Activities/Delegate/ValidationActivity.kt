package com.aait.cleanappprovider.UI.Activities.Delegate

import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R

class ValidationActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_validation_code
    lateinit var one: EditText
    lateinit var two: EditText
    lateinit var three: EditText
    lateinit var four: EditText
    lateinit var next: Button
    lateinit var resend: LinearLayout

    override fun initializeComponents() {
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        next = findViewById(R.id.next)
        resend = findViewById(R.id.resend)

    }
}