package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.R

class ServicesAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
    ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.service!!.setText(listModel.name)
        if (listModel.checked==0){
            viewHolder.check.isChecked = false
        }else{
            viewHolder.check.isChecked = true
        }
        viewHolder.service_lay.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })
        viewHolder.check.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var service=itemView.findViewById<TextView>(R.id.service)
        internal var check = itemView.findViewById<CheckBox>(R.id.check)
        internal var service_lay = itemView.findViewById<LinearLayout>(R.id.service_lay)


    }
}