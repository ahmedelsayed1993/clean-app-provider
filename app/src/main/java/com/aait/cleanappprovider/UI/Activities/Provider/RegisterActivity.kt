package com.aait.cleanappprovider.UI.Activities.Provider

import android.Manifest
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.GPS.GPSTracker
import com.aait.cleanappprovider.GPS.GpsTrakerListener
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.BaseResponse
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.ListResponse
import com.aait.cleanappprovider.Models.UserResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.LocationActivity
import com.aait.cleanappprovider.UI.Views.ListDialog
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.aait.cleanappprovider.Uitls.DialogUtil
import com.aait.cleanappprovider.Uitls.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import com.google.android.gms.common.util.ArrayUtils.removeAll
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException


class RegisterActivity:Parent_Activity(),OnItemClickListener,GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override fun onItemClick(view: View, position: Int) {

            listDialog.dismiss()
            listModel = listModels.get(position)
            city.text = listModel.name



    }

    override val layoutResource: Int
        get() = R.layout.activity_provider_register
    lateinit var ID:TextView
    lateinit var city:TextView
    var listModels = ArrayList<ListModel>()
    lateinit var listDialog:ListDialog
    lateinit var listModel: ListModel
    lateinit var shop_license:TextView
    lateinit var commercial_register:TextView
    lateinit var bank_account:TextView
    lateinit var from:TextView
    lateinit var to:TextView
    var starting = 0
    var ending = 0
    lateinit var day:TextView
    lateinit var login:LinearLayout
     var days = ArrayList<String>()
    lateinit var saturday:CheckBox
    lateinit var sunday:CheckBox
    lateinit var monday:CheckBox
    lateinit var tuesday:CheckBox
    lateinit var wednesday:CheckBox
    lateinit var thursday:CheckBox
    lateinit var friday:CheckBox
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""


    private var mAlertDialog: AlertDialog? = null
    var lat = ""
    var lng = ""
    var addresses = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath1: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
        .setRequestCode(200)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue2: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options2 = Options.init()
        .setRequestCode(300)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath2: String? = null
    internal var returnValue3: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options3 = Options.init()
        .setRequestCode(400)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue3)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    internal var returnValue4: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options4 = Options.init()
        .setRequestCode(500)                                                 //Request code for activity results
        .setCount(5)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue4)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    internal var images = java.util.ArrayList<String>()
    private var ImageBasePath3: String? = null
    var workdays = ""
    var Workday = ""
    var work = ""
    lateinit var confirm:EditText
    lateinit var view_confirm:ImageView
    lateinit var register:Button
    lateinit var view:ImageView
    lateinit var password:EditText
    lateinit var mobile:EditText
    lateinit var provider_name:EditText
    lateinit var manger_name:EditText
    lateinit var owner_name:EditText
    lateinit var phone:EditText
    lateinit var address:TextView
    lateinit var shop_images:TextView

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        city = findViewById(R.id.city)
        mobile = findViewById(R.id.mobile)
        provider_name = findViewById(R.id.provider_name)
        manger_name = findViewById(R.id.manger_name)
        owner_name = findViewById(R.id.owner_name)
        phone = findViewById(R.id.phone)
        ID = findViewById(R.id.ID)
        confirm = findViewById(R.id.confirm)
        register = findViewById(R.id.register)
        password = findViewById(R.id.password)
        view_confirm = findViewById(R.id.view_confirm)
        view = findViewById(R.id.view)
        day = findViewById(R.id.days)
        login = findViewById(R.id.login)
        shop_license = findViewById(R.id.shop_license)
        commercial_register = findViewById(R.id.commercial_register)
        bank_account = findViewById(R.id.bank_account)
        from = findViewById(R.id.from)
        to = findViewById(R.id.to)
        saturday = findViewById(R.id.saturday)
        sunday = findViewById(R.id.sunday)
        monday = findViewById(R.id.monday)
        tuesday = findViewById(R.id.tuesday)
        wednesday = findViewById(R.id.wednesday)
        thursday = findViewById(R.id.thursday)
        friday = findViewById(R.id.friday)
        address = findViewById(R.id.address)
        shop_images = findViewById(R.id.shop_images)
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}
        getLocationWithPermission()
        address.setOnClickListener {  startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1) }
        city.setOnClickListener { getCities() }
        view.setOnClickListener {  if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }
        else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        } }
        view_confirm.setOnClickListener {
            if (confirm.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                confirm.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                confirm.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
        }
        from.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)


            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)
                        starting = hourOfDay
                        var hour = hourOfDay
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        from.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                myTimeListener,
                hour,
                minute,
                true
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            timePickerDialog.show()
             }
        to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)


            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)
                        ending = hourOfDay
                        var hour = hourOfDay
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        to.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                myTimeListener,
                hour,
                minute,
                true
            )
            timePickerDialog.setTitle(getString(R.string.to))
            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            timePickerDialog.show()
        }

        ID.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        shop_license.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        shop_images.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options4)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options4)
            }
        }

        commercial_register.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        bank_account.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options3)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options3)
            }
        }

        saturday.setOnClickListener {
            if (!saturday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("saturday")
                }
            }else{
                days.add("saturday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        sunday.setOnClickListener {
            if (!sunday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("sunday")
                }
            }else{
                days.add("sunday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        monday.setOnClickListener {
            if (!monday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("monday")
                }
            }else{
                days.add("monday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        tuesday.setOnClickListener {
            if (!tuesday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("tuesday")
                }
            }else{
                days.add("tuesday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        wednesday.setOnClickListener {
            if (!wednesday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("wednesday")
                }
            }else{
                days.add("wednesday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        thursday.setOnClickListener {
            if (!thursday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("thursday")
                }
            }else{
                days.add("thursday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        friday.setOnClickListener {
            if (!friday.isChecked){
                if (days.isEmpty()){

                }else{
                    days.remove("friday")
                }
            }else{
                days.add("friday")
            }
            workdays = Gson().toJson(days).replace("[","")
            Workday = workdays.replace("]","")
            work = days.joinToString(prefix = "",separator = ",",postfix = "")
            Log.e("days",work)
        }
        register.setOnClickListener {
            if (CommonUtil.checkEditError(mobile,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(mobile,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(provider_name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(manger_name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(manger_name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(owner_name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError(city,getString(R.string.Please_specify_the_housing_address_in_detail))||
                CommonUtil.checkTextError(address,getString(R.string.enter_location))||
                    CommonUtil.checkTextError(ID,getString(R.string.enter_image))||
                CommonUtil.checkTextError(shop_images,getString(R.string.enter_image))||
                    CommonUtil.checkTextError(shop_license,getString(R.string.enter_image))||
                    CommonUtil.checkTextError(commercial_register,getString(R.string.enter_image))||
                    CommonUtil.checkTextError(bank_account,getString(R.string.enter_image))||
                    CommonUtil.checkTextError(from,getString(R.string.from))||
                    CommonUtil.checkTextError(to,getString(R.string.to))){
                return@setOnClickListener
            }else{
                if (ending<starting){
                   CommonUtil.makeToast(mContext,getString(R.string.start_greater_end))
                }else{
                    if (days.isEmpty()){
                        day.error = getString(R.string.work_days)
                    }else{
                        if (CommonUtil.checkEditError(password,getString(R.string.enter_password))||
                                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                                CommonUtil.checkEditError(confirm,getString(R.string.enter_password))){
                            return@setOnClickListener
                        }else{
                            if (!password.text.toString().equals(confirm.text.toString())){
                                CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
                            }else{
                                register(ImageBasePath!!,ImageBasePath1!!,ImageBasePath2!!,ImageBasePath3!!,images)
                            }
                        }
                    }
                }
            }

        }

    }



    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listModels = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@RegisterActivity,listModels,getString(R.string.city))
                        listDialog.show()
                    }
                }
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath != null) {

                ID.text = getString(R.string.Image_attached)
            }
        }
        else  if (requestCode == 200) {
            returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath1 = returnValue1!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath1 != null) {

                shop_license.text = getString(R.string.Image_attached)
            }
        }else if (requestCode == 300) {
            returnValue2 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath2 = returnValue2!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath2 != null) {

                commercial_register.text = getString(R.string.Image_attached)
            }
        }else if (requestCode == 400) {
            returnValue3 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath3 = returnValue3!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath3 != null) {

                bank_account.text = getString(R.string.Image_attached)
            }
        }else if (requestCode == 500) {
            returnValue4 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)
            for (i in 0..returnValue4?.size!! -1){
                images.add(returnValue4!![i])
            }



            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
             Log.e("size",Gson().toJson(images))
            if (!images.isEmpty()) {

                shop_images.text = getString(R.string.Image_attached)
            }
        }else{
            if (data?.getStringExtra("result")!=null) {
                addresses = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = addresses
            }else{
                addresses = ""
                lat = ""
                lng = ""
                address.text = addresses
            }

        }
    }

    fun register(path1:String,path2:String,path3:String,path4:String,imges:ArrayList<String>){
        var imgs = ArrayList<MultipartBody.Part>()
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path1)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("personal_id", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path2)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("shop_license", ImageFile1.name, fileBody1)
        var filePart2: MultipartBody.Part? = null
        val ImageFile2 = File(path3)
        val fileBody2 = RequestBody.create(MediaType.parse("*/*"), ImageFile2)
        filePart2 = MultipartBody.Part.createFormData("commercial", ImageFile2.name, fileBody2)
        var filePart3: MultipartBody.Part? = null
        val ImageFile3 = File(path4)
        val fileBody3 = RequestBody.create(MediaType.parse("*/*"), ImageFile3)
        filePart3 = MultipartBody.Part.createFormData("bank_account", ImageFile3.name, fileBody3)
        for (i in 0..imges.size-1){
            var filePart4: MultipartBody.Part? = null
            val ImageFile4 = File(imges.get(i))
            val fileBody4 = RequestBody.create(MediaType.parse("*/*"), ImageFile4)
            filePart4 = MultipartBody.Part.createFormData("place_images[]", ImageFile4.name, fileBody4)
            imgs.add(filePart4)
        }
        Client.getClient()?.create(Service::class.java)?.signUpProvider(mobile.text.toString(),provider_name.text.toString(),password.text.toString()
        ,deviceID,"android",manger_name.text.toString(),owner_name.text.toString(),
            phone.text.toString(),listModel.id!!,addresses,lat,lng,filePart,filePart1,filePart2,filePart3,from.text.toString(),to.text.toString(),work,imgs,mLanguagePrefManager.appLanguage)?.enqueue(
            object :Callback<UserResponse>{
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@RegisterActivity,ActivateActivity::class.java)
                            intent.putExtra("user",response.body()?.data)
                            startActivity(intent)
                            this@RegisterActivity.finish()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 700)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
//                        Toast.makeText(
//                            mContext,
//                            resources.getString(R.string.detect_location),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))




                    }

                } catch (e: IOException) {
                }

                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}