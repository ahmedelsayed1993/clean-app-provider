package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.ChooseCatResponse
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.ListResponse
import com.aait.cleanappprovider.Models.UserModel
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.CatsAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoriesActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
       if (view.id == R.id.category) {
           cat(categories.get(position).id!!)
       }

    }

    override val layoutResource: Int
        get() = R.layout.activity_categories
    lateinit var cats:RecyclerView
    var categories = ArrayList<ListModel>()
    lateinit var catsAdapter: CatsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        cats = findViewById(R.id.cats)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        catsAdapter = CatsAdapter(mContext,categories,R.layout.recycler_cats)
        catsAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = catsAdapter
        getCats()

    }

    fun getCats(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.categories(mLanguagePrefManager.appLanguage,userModel.id!!)?.enqueue(object :
            Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        catsAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun cat(category:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.chooseCategory(mLanguagePrefManager.appLanguage,userModel.id!!,category)?.enqueue(object :Callback<ChooseCatResponse>{
            override fun onFailure(call: Call<ChooseCatResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ChooseCatResponse>,
                response: Response<ChooseCatResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        if (response.body()?.active!!.equals("0")){
                            startActivity(Intent(this@CategoriesActivity,LoginActivity::class.java))
                            finish()
                        }else if (response.body()?.active!!.equals("1")){
                            if(response.body()?.category_key.equals("clothes")) {
                                val intent =
                                    Intent(this@CategoriesActivity, SubCategoryActivity::class.java)
                                intent.putExtra("user", userModel.id!!)
                                intent.putExtra("category_id", response.body()?.category_id)
                                intent.putExtra("category_name", response.body()?.category_name)
                                startActivity(intent)
                                finish()
                            }else if (response.body()?.category_key.equals("locations")){
                                val intent = Intent(this@CategoriesActivity, LocationsActivity::class.java)
                                intent.putExtra("user", userModel.id!!)
                                intent.putExtra("category_id", response.body()?.category_id)
                                intent.putExtra("category_name", response.body()?.category_name)
                                startActivity(intent)
                                finish()
                            }else if (response.body()?.category_key.equals("cars")){
                                val intent = Intent(this@CategoriesActivity, CarsActivity::class.java)
                                intent.putExtra("user", userModel.id!!)
                                intent.putExtra("category_id", response.body()?.category_id)
                                intent.putExtra("category_name", response.body()?.category_name)
                                startActivity(intent)
                                finish()
                            }

                        }
                    }
                }
            }

        })
    }
}