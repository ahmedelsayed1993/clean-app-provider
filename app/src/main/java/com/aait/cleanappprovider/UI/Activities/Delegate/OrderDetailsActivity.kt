package com.aait.cleanappprovider.UI.Activities.Delegate

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.GPS.GPSTracker
import com.aait.cleanappprovider.GPS.GpsTrakerListener
import com.aait.cleanappprovider.Models.OrderDetailsModel
import com.aait.cleanappprovider.Models.OrderDetailsResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Delegate.MainActivity
import com.aait.cleanappprovider.UI.Activities.Delegate.InvoiceActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.aait.cleanappprovider.Uitls.DialogUtil
import com.aait.cleanappprovider.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class OrderDetailsActivity :Parent_Activity(),GpsTrakerListener{
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    override val layoutResource: Int
        get() = R.layout.activity_order_details_delegate

    var id = 0
    lateinit var Image: ImageView
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    lateinit var num:TextView
    lateinit var client_name:TextView


    private var mAlertDialog: AlertDialog? = null
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var invoice: TextView
    lateinit var location: TextView
    lateinit var pay: TextView
    lateinit var service: TextView
    lateinit var service_type: TextView
    lateinit var notes: TextView
    lateinit var accept: Button
    lateinit var refuse: Button
    var order_id = 0
    var lat = ""
    var lng = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        order_id = intent.getIntExtra("order_id",0)
        Image = findViewById(R.id.Image)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        invoice = findViewById(R.id.invoice)
        location = findViewById(R.id.location)
        pay = findViewById(R.id.pay)
        service = findViewById(R.id.service)
        service_type = findViewById(R.id.service_type)
        notes = findViewById(R.id.notes)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        num = findViewById(R.id.num)
        client_name = findViewById(R.id.client_name)
        title.text = getString(R.string.booking_details)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish()}
        getLocationWithPermission()
        getOrder()
        location.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang+"&daddr="+lat+","+lng)
                )
            ) }

        accept.setOnClickListener { accept("accepted",null) }
        refuse.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_refuse)
            val reason = dialog?.findViewById<EditText>(R.id.reason)
            val send = dialog?.findViewById<Button>(R.id.send)
            send?.setOnClickListener {
                if (CommonUtil.checkEditError(reason,getString(R.string.reason))){
                    return@setOnClickListener
                }else{
                    accept("refuse",reason.text.toString())
                }
            }
            dialog?.show()
        }
        invoice.setOnClickListener { val intent = Intent(this, InvoiceActivity::class.java)
            intent.putExtra("id",order_id)
            startActivity(intent)}
    }
    fun setData(orderDetailsModel: OrderDetailsModel){
        Glide.with(mContext).load(orderDetailsModel.image).into(Image)
        name.text = orderDetailsModel.username
        lat = orderDetailsModel.lat!!
        lng = orderDetailsModel.lng!!
        num.text = orderDetailsModel.id.toString()
        client_name.text = orderDetailsModel.name
        for (i in 0..orderDetailsModel.items!!.size-1){
            if (orderDetailsModel.items!!.get(i).title.equals("address")){
                address.text = orderDetailsModel.items!!.get(i).details
                location.text = orderDetailsModel.items!!.get(i).details
            }else if (orderDetailsModel.items!!.get(i).title.equals("payment")){
                pay.text = orderDetailsModel.items!!.get(i).details
            }
            else if (orderDetailsModel.items!!.get(i).title.equals("notes")){
                notes.text = orderDetailsModel.items!!.get(i).details
            }
            else if (orderDetailsModel.items!!.get(i).title.equals("services")){
                service.text = orderDetailsModel.items!!.get(i).details
            }
        }


    }

    fun getOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DetailsOrder(mLanguagePrefManager.appLanguage,id,order_id,mSharedPrefManager.userData.id!!
            ,null,"delegate",null)?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun accept(status:String,reason:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DetailsOrder(mLanguagePrefManager.appLanguage,id,order_id,mSharedPrefManager.userData.id!!
            ,status,"delegate",reason)?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@OrderDetailsActivity,BackToMainActivity::class.java))
                        finish()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
//                        Toast.makeText(
//                            mContext,
//                            resources.getString(R.string.detect_location),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}