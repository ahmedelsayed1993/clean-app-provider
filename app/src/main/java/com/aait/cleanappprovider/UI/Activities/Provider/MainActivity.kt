package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.SplashActivity
import com.aait.cleanappprovider.UI.Adapters.DelegatesAdapter
import com.aait.cleanappprovider.UI.Adapters.NewOrdersAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_provider_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this@MainActivity,OrderDetailsActivity::class.java)
        intent.putExtra("id",orderModels.get(position).id)
        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_provider_main
    lateinit var main:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var orders:LinearLayout
    lateinit var delegates:LinearLayout
    lateinit var services:LinearLayout
    lateinit var about_app:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var complains:LinearLayout
    lateinit var settings:LinearLayout
    lateinit var share_app:LinearLayout
    lateinit var logout:LinearLayout
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var name:TextView
    lateinit var image:CircleImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var notification:ImageView
    internal var layNoItem: RelativeLayout? = null
    lateinit var count:TextView
    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var orderModels = java.util.ArrayList<OrderModel>()
    internal lateinit var orderAdapter: NewOrdersAdapter

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        count = findViewById(R.id.count)
        title.text = getString(R.string.main)
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        finish()}
        sideMenu()
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderAdapter =  NewOrdersAdapter(mContext,orderModels,R.layout.recycler_order)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome()
        getCount()}
        getHome()
        getCount()
    }
    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 80f)
        drawer_layout.setRadius(Gravity.END, 80f)
        drawer_layout.setViewScale(Gravity.START, 0.7f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.7f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 80f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 80f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        name = drawer_layout.findViewById(R.id.name)
        image = drawer_layout.findViewById(R.id.image)
        main = drawer_layout.findViewById(R.id.main)
        profile = drawer_layout.findViewById(R.id.profile)
        orders = drawer_layout.findViewById(R.id.orders)
        delegates = drawer_layout.findViewById(R.id.delegates)
        services = drawer_layout.findViewById(R.id.services)
        about_app = drawer_layout.findViewById(R.id.about_app)
        terms = drawer_layout.findViewById(R.id.terms)
        complains = drawer_layout.findViewById(R.id.complains)
        settings = drawer_layout.findViewById(R.id.settings)
        share_app = drawer_layout.findViewById(R.id.share_app)
        logout = drawer_layout.findViewById(R.id.logout)
        name.text = mSharedPrefManager.userData.name!!
        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java)) }
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity,AboutAppActivity::class.java)) }
        terms.setOnClickListener { startActivity(Intent(this@MainActivity,TermsActivity::class.java)) }
        share_app.setOnClickListener { CommonUtil.ShareApp(mContext) }
        complains.setOnClickListener { startActivity(Intent(this@MainActivity,ComplainsActivity::class.java)) }
        settings.setOnClickListener { startActivity(Intent(this@MainActivity,SettingsActivity::class.java)) }
        delegates.setOnClickListener { startActivity(Intent(this@MainActivity,DelegatesActivity::class.java))
        }
        services.setOnClickListener {
            if (mSharedPrefManager.userData.category_key.equals("cars")){
                startActivity(Intent(this,CarServicesActivity::class.java))
            }else if (mSharedPrefManager.userData.category_key.equals("locations")) {
                startActivity(Intent(this, ServicesActivity::class.java))
            }else if (mSharedPrefManager.userData.category_key.equals("clothes")) {
                startActivity(Intent(this, ClothesServicesActivity::class.java))
            }
        }
        main.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        orders.setOnClickListener { startActivity(Intent(this@MainActivity,OrdersActivity::class.java)) }
        logout.setOnClickListener { logout() }
    }

    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.orders(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"current")?.enqueue(object :
            Callback<OrderResponse> {
            override fun onResponse(call: Call<OrderResponse>, response: Response<OrderResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                orderAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"android",mSharedPrefManager.userData.device_id!!)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {

                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getCount(){
        Client.getClient()?.create(Service::class.java)?.UnRead(mSharedPrefManager.userData.id!!)?.enqueue(object :Callback<SwitchNotification>{
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){


                            count.text = response.body()?.data!!.toString()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }

            }
        })
    }
}