package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.DelegateModel
import com.aait.cleanappprovider.Models.ServiceModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class DelegatesAdapter (context: Context, data: MutableList<DelegateModel>, layoutId: Int) :
    ParentRecyclerAdapter<DelegateModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)
        viewHolder.rating!!.rating = listModel.rate!!
        Glide.with(mcontext).asBitmap().load(listModel.avatar).into(viewHolder.image)
        if (listModel.active==0){
            viewHolder.status.visibility = View.VISIBLE
            viewHolder.status.text = mcontext.getString(R.string.blocked)
        }else if (listModel.confirm==0){
            viewHolder.status.visibility = View.VISIBLE
            viewHolder.status.text = mcontext.getString(R.string.inactive)
        }
        else if (listModel.active==1&&listModel.confirm==0){
            viewHolder.status.visibility = View.VISIBLE
            viewHolder.status.text = mcontext.getString(R.string.inactive)
        }else{
            viewHolder.status.visibility = View.GONE
        }
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)
        internal var status = itemView.findViewById<TextView>(R.id.status)



    }
}