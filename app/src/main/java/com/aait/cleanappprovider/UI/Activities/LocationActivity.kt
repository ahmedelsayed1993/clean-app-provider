package com.aait.cleanappprovider.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.GPS.GPSTracker
import com.aait.cleanappprovider.GPS.GpsTrakerListener
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.aait.cleanappprovider.Uitls.DialogUtil
import com.aait.cleanappprovider.Uitls.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException
import java.util.*

class LocationActivity : Parent_Activity(), OnMapReadyCallback, GoogleMap.OnMapClickListener,
    GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_location

    lateinit var back:ImageView
    lateinit var map:MapView
    lateinit var done:Button
    override fun onMapClick(p0: LatLng?) {
        Log.e("LatLng", p0.toString())
        mLang =  java.lang.Double.toString(p0?.latitude!!) .toString()
        mLat =  java.lang.Double.toString(p0?.longitude!!) .toString()
        if (myMarker != null) {
            myMarker.remove()
            putMapMarker1(p0?.latitude, p0?.longitude)
        } else {
            putMapMarker1(p0?.latitude, p0?.longitude)
        }

        if (p0?.latitude != 0.0 && p0?.longitude != 0.0) {
            putMapMarker1(p0?.latitude, p0?.longitude)
            mLat = p0?.latitude.toString()
            mLang = p0?.longitude.toString()
            val addresses: List<Address>

            geocoder = Geocoder(this@LocationActivity, Locale.getDefault())

            try {
                addresses = geocoder.getFromLocation(
                    java.lang.Double.parseDouble(mLat),
                    java.lang.Double.parseDouble(mLang),
                    1
                )

                if (addresses.isEmpty()) {
                    Toast.makeText(
                        this@LocationActivity,
                        resources.getString(R.string.detect_location),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    result = addresses[0].getAddressLine(0)
                    Log.e("address",result)
                     CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
                }


            } catch (e: IOException) {
            }

            googleMap.clear()
            putMapMarker1(p0?.latitude, p0?.longitude)
        }

    }

    fun putMapMarker1(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.map))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        // CameraUpdateFactory.newLatLngZoom(latLng, 100f)
        // kkjgj

    }


    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""


    private var mAlertDialog: AlertDialog? = null
    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        googleMap.setOnMapClickListener(this)
        getLocationWithPermission()
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        map = findViewById(R.id.map)
        done = findViewById(R.id.done)
        back.setOnClickListener { onBackPressed()
        this.finish()}
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        done.setOnClickListener {
            if (!result.equals("")) {
                val returnIntent = Intent()
                returnIntent.putExtra("result", result)
                returnIntent.putExtra("lat", mLat)
                returnIntent.putExtra("lng", mLang)
                setResult(1, returnIntent)
                finish()
            }else{

            }
        }

    }

    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.map))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
                putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}