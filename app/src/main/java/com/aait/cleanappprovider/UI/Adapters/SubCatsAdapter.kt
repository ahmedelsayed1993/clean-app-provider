package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.SubCategoryModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class SubCatsAdapter (context: Context, data: MutableList<SubCategoryModel>, layoutId: Int) :
    ParentRecyclerAdapter<SubCategoryModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected = 0

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val subCategoryModel = data.get(position)
        viewHolder.name!!.setText(subCategoryModel.name)
        Glide.with(mcontext).asBitmap().load(subCategoryModel.image).into(viewHolder.image)
        viewHolder.selected.isChecked = (selected==position)
        viewHolder.selected.tag = selected



        viewHolder.selected.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var sub_lay = itemView.findViewById<LinearLayout>(R.id.sub_lay)
        internal var selected = itemView.findViewById<CheckBox>(R.id.selected)


    }
}