package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ProductModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class AirAdapter (context: Context, data: MutableList<ProductModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProductModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var android.widget.TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        viewHolder.name!!.setText(productModel?.name)
        Glide.with(mcontext).asBitmap().load(productModel.image).into(viewHolder.image)
        if (viewHolder.check.isChecked){
            viewHolder.price.visibility = View.VISIBLE
            productModel.selected = true
            // notifyDataSetChanged()
        }else{
            viewHolder.price.visibility = View.GONE
            productModel.selected = false
            // notifyDataSetChanged()
        }




        viewHolder.price.addTextChangedListener (object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                productModel.price = charSequence.toString()



            }

            override fun afterTextChanged(editable: Editable) {}
        })
        viewHolder.check.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var price = itemView.findViewById<EditText>(R.id.price)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var check = itemView.findViewById<CheckBox>(R.id.check)
//        internal fun ViewHolder(itemView: View) {
//
//            setClickableRootView(itemView)
////            addServiceAdapter = AddServiceAdapter(mcontext)
//            services.setLayoutManager(
//                LinearLayoutManager(
//                    mcontext,
//                    LinearLayoutManager.HORIZONTAL,
//                    false
//                )
//            )
//
//            services.adapter = addServiceAdapter
//        }



    }
}


