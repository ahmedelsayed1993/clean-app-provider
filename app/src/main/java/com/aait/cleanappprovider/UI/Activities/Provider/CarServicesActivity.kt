package com.aait.cleanappprovider.UI.Activities.Provider

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.CarServiceAdapter
import com.aait.cleanappprovider.UI.Adapters.CarsServicesAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarServicesActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.edit){
            if (carServiceAdapter.getData().get(position)?.price.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.enter_price))
            }else {
                Update(
                    carServiceAdapter.getData().get(position)?.id!!,
                    0,
                    carServiceAdapter.getData().get(position)?.price!!
                )

            }
        }else if (view.id == R.id.delete){
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.delete_dialog)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener { dialog?.dismiss() }
            confirm.setOnClickListener {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.deleteService(
                    mLanguagePrefManager.appLanguage, mSharedPrefManager.userData.id!!
                    , locationServiceModels.get(position).id!!
                )?.enqueue(object : Callback<AboutAppResponse> {
                    override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(
                        call: Call<AboutAppResponse>,
                        response: Response<AboutAppResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.value.equals("1")) {
                                CommonUtil.makeToast(mContext, response.body()?.data!!)
                                dialog?.dismiss()
                                Services()
                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
            dialog?.show()
        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_car_services
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var lay:LinearLayout
    lateinit var sizes:RecyclerView
    lateinit var services:RecyclerView
    lateinit var no_services:LinearLayout
    lateinit var add:Button
    lateinit var carsServicesAdapter: CarsServicesAdapter
    lateinit var carServiceAdapter: CarServiceAdapter
    var carServicesModels = ArrayList<CarServiceModel>()
    var locationServiceModels = ArrayList<LocationServiceModel>()
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var add_service:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        lay = findViewById(R.id.lay)
        sizes = findViewById(R.id.sizes)
        services = findViewById(R.id.services)
        no_services = findViewById(R.id.no_services)
        add = findViewById(R.id.add)
        add_service = findViewById(R.id.add_service)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        title.text = mSharedPrefManager.userData.category_name
        gridLayoutManager = GridLayoutManager(mContext,2)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        carServiceAdapter = CarServiceAdapter(mContext,locationServiceModels,R.layout.recycler_car)
        carServiceAdapter.setOnItemClickListener(this)
        carsServicesAdapter = CarsServicesAdapter(mContext,carServicesModels,R.layout.service_cars)
        services.layoutManager = linearLayoutManager
        sizes.layoutManager = gridLayoutManager
        services.adapter = carsServicesAdapter
        sizes.adapter = carServiceAdapter
        Services()
        add.setOnClickListener {
            val intent = Intent(this@CarServicesActivity, CarsActivity::class.java)
            intent.putExtra("user", mSharedPrefManager.userData.id!!)
            intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
            intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
            startActivity(intent)

        }
        add_service.setOnClickListener {
            val intent = Intent(this@CarServicesActivity, CarsActivity::class.java)
            intent.putExtra("user", mSharedPrefManager.userData.id!!)
            intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
            intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
            startActivity(intent)

        }


    }

    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
        Client.getClient()?.create(Service::class.java)?.carServices(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(
            object : Callback<CarServiceResponse> {
                override fun onFailure(call: Call<CarServiceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<CarServiceResponse>,
                    response: Response<CarServiceResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                lay.visibility = View.GONE
                                no_services.visibility =  View.VISIBLE

                            }else{
                                lay.visibility = View.VISIBLE
                                no_services.visibility =  View.GONE
                                carServiceAdapter.updateAll(response.body()?.data!!)
                                carsServicesAdapter.updateAll(response.body()?.subcategory_price!!)
                            }
                        }
                    }
                }

            }
        )
    }

    fun Services(){
        hideKeyboard()
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.carServices(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(
            object : Callback<CarServiceResponse> {
                override fun onFailure(call: Call<CarServiceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<CarServiceResponse>,
                    response: Response<CarServiceResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                lay.visibility = View.GONE
                                no_services.visibility =  View.VISIBLE

                            }else{
                                lay.visibility = View.VISIBLE
                                no_services.visibility =  View.GONE
                                carServiceAdapter.updateAll(response.body()?.data!!)
                                carsServicesAdapter.updateAll(response.body()?.subcategory_price!!)
                            }
                        }
                    }
                }

            }
        )
    }
    fun Update(product:Int,services:Int,price:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.updatePrice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,product,services,price)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<AboutAppResponse>, response: Response<AboutAppResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        Services()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}