package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.DelegateDetailsResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditDelegateActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_edit_delegate

    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var name: EditText
    lateinit var description: EditText
    lateinit var phone: EditText
    lateinit var password: EditText
    lateinit var add: Button
   var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        description = findViewById(R.id.description)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        add = findViewById(R.id.add)
        back.setOnClickListener { val intent = Intent(this@EditDelegateActivity,DelegateDetailsActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        finish()}
        title.text  = getString(R.string.edit)
        getData()

        add.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(description,getString(R.string.enter_description))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)){
                return@setOnClickListener
            }else{
                Edit()
            }
        }


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DelegateDetails(mLanguagePrefManager.appLanguage,id)?.enqueue(object :
            Callback<DelegateDetailsResponse> {
            override fun onFailure(call: Call<DelegateDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<DelegateDetailsResponse>,
                response: Response<DelegateDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        name.setText(response.body()?.data?.name)
                        phone.setText(response.body()?.data?.phone)
                        password.setText(response.body()?.data?.password)
                        description.setText(response.body()?.data?.description)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })

    }
    fun Edit(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditDelegate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,
            name.text.toString(),description.text.toString(),phone.text.toString(),password.text.toString())?.enqueue(object :Callback<DelegateDetailsResponse>{
            override fun onFailure(call: Call<DelegateDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<DelegateDetailsResponse>,
                response: Response<DelegateDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@EditDelegateActivity,DelegateDetailsActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@EditDelegateActivity,DelegateDetailsActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)
        finish()
    }
}