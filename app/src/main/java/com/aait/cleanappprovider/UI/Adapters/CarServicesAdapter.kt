package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.ProductModel
import com.aait.cleanappprovider.Models.ServiceModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class CarServicesAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
    ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var android.widget.TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        viewHolder.name!!.setText(productModel?.name)
        viewHolder.price.visibility = View.GONE
        viewHolder.check.setOnClickListener {  if (viewHolder.check.isChecked){
            viewHolder.price.visibility = View.VISIBLE
            viewHolder.price.requestFocus()
            productModel.checked = 1
        }else{
            viewHolder.price.visibility = View.GONE
            productModel.checked = 0
        } }

        viewHolder.itemView.setOnClickListener {  if (viewHolder.check.isChecked){
            viewHolder.price.visibility = View.VISIBLE
            viewHolder.price.requestFocus()
            productModel.checked = 1
        }else{
            viewHolder.price.visibility = View.GONE
            productModel.checked = 0
        } }


       // viewHolder.price.setText("")
        viewHolder.price.addTextChangedListener (object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                productModel?.price = charSequence.toString()



            }

            override fun afterTextChanged(editable: Editable) {}
        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var price = itemView.findViewById<EditText>(R.id.price)
        internal var check = itemView.findViewById<CheckBox>(R.id.check)




    }
}


