package com.aait.cleanappprovider.UI.Activities.Delegate

import android.content.Intent
import android.os.Build
import android.text.InputType
import android.widget.*
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.UserResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Delegate.ValidationActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.aait.cleanappprovider.Uitls.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RegisterActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_register

    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var another_phone:EditText
    lateinit var home_phone:EditText
    lateinit var ID:TextView
    lateinit var profile:TextView
    lateinit var medical:TextView
    lateinit var home_address:EditText
    lateinit var bank_account:TextView
    lateinit var license:TextView
    lateinit var car_image:TextView
    lateinit var model:EditText
    lateinit var password:EditText
    lateinit var confirm_password:EditText
    lateinit var view:ImageView
    lateinit var view_confirm:ImageView
    lateinit var register:Button
    lateinit var login:LinearLayout
    lateinit var form:TextView
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath1: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
        .setRequestCode(200)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue2: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options2 = Options.init()
        .setRequestCode(300)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath2: String? = null
    internal var returnValue3: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options3 = Options.init()
        .setRequestCode(400)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue3)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath3: String? = null
    internal var returnValue4: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options4 = Options.init()
        .setRequestCode(500)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue4)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath4: String? = null
    internal var returnValue5: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options5 = Options.init()
        .setRequestCode(600)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue5)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath5: String? = null
    internal var returnValue6: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options6 = Options.init()
        .setRequestCode(700)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue6)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath6: String? = null

    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        another_phone = findViewById(R.id.another_phone)
        home_phone = findViewById(R.id.home_phone)
        ID = findViewById(R.id.ID)
        profile = findViewById(R.id.profile)
        medical = findViewById(R.id.medical)
        home_address = findViewById(R.id.home_address)
        bank_account = findViewById(R.id.bank_account)
        license = findViewById(R.id.license)
        car_image = findViewById(R.id.car_image)
        model = findViewById(R.id.model)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_password)
        view = findViewById(R.id.view)
        view_confirm = findViewById(R.id.view_confirm)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        form = findViewById(R.id.form)

        ID.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        profile.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        medical.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        bank_account.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options3)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options3)
            }
        }
        license.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options4)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options4)
            }
        }
        form.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options5)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options5)
            }
        }
        car_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options6)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options6)
            }
        }

        register.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(another_phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(another_phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(home_phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(home_phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError(ID,getString(R.string.enter_image))||
                    CommonUtil.checkTextError(profile,getString(R.string.enter_image))||
                    CommonUtil.checkTextError(medical,getString(R.string.enter_image))||
                    CommonUtil.checkEditError(home_address,getString(R.string.Please_specify_the_housing_address_in_detail))||
                    CommonUtil.checkTextError(bank_account,getString(R.string.Please_enter_the_account_picture))||
                    CommonUtil.checkTextError(license,getString(R.string.Please_enter_the_license_image))||
                    CommonUtil.checkTextError(form,getString(R.string.Please_enter_the_form_image))||
                    CommonUtil.checkTextError(car_image,getString(R.string.Please_enter_the_car_image))||
                    CommonUtil.checkTextError(model,getString(R.string.Please_select_the_vehicle_type))||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())){
                    CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
                }else{

                    register(ImageBasePath!!,ImageBasePath1!!,ImageBasePath2!!,ImageBasePath3!!,ImageBasePath4!!,ImageBasePath5!!,ImageBasePath6!!)
                }
            }
        }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        view_confirm.setOnClickListener {
            if (confirm_password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                confirm_password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                confirm_password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }
        view.setOnClickListener {
            if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath != null) {

                ID.text = getString(R.string.Image_attached)
            }
        }
        else  if (requestCode == 200) {
            returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath1 = returnValue1!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath1 != null) {

                profile.text = getString(R.string.Image_attached)
            }
        }else if (requestCode == 300) {
            returnValue2 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath2 = returnValue2!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath2 != null) {

                medical.text = getString(R.string.Image_attached)
            }
        }else if (requestCode == 400) {
            returnValue3 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath3 = returnValue3!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath3 != null) {

                bank_account.text = getString(R.string.Image_attached)
            }
        }
        else if (requestCode == 500) {
            returnValue4 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath4 = returnValue4!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath4 != null) {

                license.text = getString(R.string.Image_attached)
            }
        }
        else if (requestCode == 600) {
            returnValue5 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath5 = returnValue5!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath5 != null) {

                form.text = getString(R.string.Image_attached)
            }
        }
        else if (requestCode == 700) {
            returnValue6 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath6 = returnValue6!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

            if (ImageBasePath6 != null) {

                car_image.text = getString(R.string.Image_attached)
            }
        }
    }

    fun register(path:String,path1:String,path2:String,path3:String,path4:String,path5:String,path6:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("personal_id", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("avatar", ImageFile1.name, fileBody1)
        var filePart2: MultipartBody.Part? = null
        val ImageFile2 = File(path2)
        val fileBody2 = RequestBody.create(MediaType.parse("*/*"), ImageFile2)
        filePart2 = MultipartBody.Part.createFormData("checkup_medical", ImageFile2.name, fileBody2)
        var filePart3: MultipartBody.Part? = null
        val ImageFile3 = File(path3)
        val fileBody3 = RequestBody.create(MediaType.parse("*/*"), ImageFile3)
        filePart3 = MultipartBody.Part.createFormData("bank_account", ImageFile3.name, fileBody3)
        var filePart4: MultipartBody.Part? = null
        val ImageFile4 = File(path4)
        val fileBody4 = RequestBody.create(MediaType.parse("*/*"), ImageFile4)
        filePart4 = MultipartBody.Part.createFormData("license_image", ImageFile4.name, fileBody4)
        var filePart5: MultipartBody.Part? = null
        val ImageFile5 = File(path5)
        val fileBody5 = RequestBody.create(MediaType.parse("*/*"), ImageFile5)
        filePart5 = MultipartBody.Part.createFormData("image_form", ImageFile5.name, fileBody5)
        var filePart6: MultipartBody.Part? = null
        val ImageFile6 = File(path6)
        val fileBody6 = RequestBody.create(MediaType.parse("*/*"), ImageFile6)
        filePart6 = MultipartBody.Part.createFormData("car_image", ImageFile6.name, fileBody6)
        Client.getClient()?.create(Service::class.java)?.singUpDelegate(phone.text.toString(),name.text.toString(),password.text.toString()
        ,"dfghjkl./,mnbv","android",another_phone.text.toString(),home_phone.text.toString(),home_address.text.toString()
        ,filePart,filePart1,filePart2,filePart3,filePart4,filePart5,filePart6,model.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity, AcivateActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        this@RegisterActivity.finish()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}