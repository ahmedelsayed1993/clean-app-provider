package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.NotificationModel
import com.aait.cleanappprovider.R
import com.daimajia.swipe.SwipeLayout

class NotificationAdapter (context: Context, data: MutableList<NotificationModel>, layoutId: Int) :
    ParentRecyclerAdapter<NotificationModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.title!!.setText(listModel.content)
        viewHolder.swipe.addDrag(
            SwipeLayout.DragEdge.Right,
            viewHolder.swipe.findViewById(R.id.bottom_wraper)
        )
        viewHolder.swipe.addSwipeListener(object : SwipeLayout.SwipeListener {
            override fun onStartOpen(layout: SwipeLayout) {

            }

            override fun onOpen(layout: SwipeLayout) {

            }

            override fun onStartClose(layout: SwipeLayout) {

            }

            override fun onClose(layout: SwipeLayout) {

            }

            override fun onUpdate(layout: SwipeLayout, leftOffset: Int, topOffset: Int) {

            }

            override fun onHandRelease(layout: SwipeLayout, xvel: Float, yvel: Float) {

            }
        })
        viewHolder.delete.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })
        viewHolder.lay.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })


    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var title=itemView.findViewById<TextView>(R.id.title)
        internal var swipe=itemView.findViewById<SwipeLayout>(R.id.swipe)
        internal  var delete=itemView.findViewById<TextView>(R.id.delete)
        internal var lay = itemView.findViewById<LinearLayout>(R.id.lay)
    }
}