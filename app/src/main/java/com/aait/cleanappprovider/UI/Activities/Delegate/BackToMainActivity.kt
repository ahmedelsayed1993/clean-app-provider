package com.aait.cleanappprovider.UI.Activities.Delegate

import android.content.Intent
import android.widget.Button
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R

class BackToMainActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_back_to_main_delegate
    lateinit var back:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}