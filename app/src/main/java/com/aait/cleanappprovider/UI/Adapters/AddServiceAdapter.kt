package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.ServiceModel
import com.aait.cleanappprovider.R

class AddServiceAdapter (context: Context,data:MutableList<ServiceModel>) :
    ParentRecyclerAdapter<ServiceModel>(context,data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_servce, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val serviceModel = data.get(position)


        viewHolder.service4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                // productModel.services?.get(0)?.price = charSequence.toString()
                serviceModel.service_id = serviceModel.service_id
                serviceModel.price = charSequence.toString()


            }

            override fun afterTextChanged(editable: Editable) {}
        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {






        internal var service4 = itemView.findViewById<EditText>(R.id.service4)



    }
}