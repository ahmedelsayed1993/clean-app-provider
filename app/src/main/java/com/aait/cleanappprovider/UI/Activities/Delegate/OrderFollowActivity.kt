package com.aait.cleanappprovider.UI.Activities.Delegate

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.TrackOrderDelegateResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderFollowActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_follow
    lateinit var one: ImageView
    lateinit var two: ImageView
    lateinit var three: ImageView
    lateinit var received: Button
    lateinit var work_underway: Button

    lateinit var one1: ImageView
    lateinit var two1: ImageView
    lateinit var three1: ImageView
    lateinit var received1: Button
    lateinit var work_underway1: Button
    lateinit var back_to_main: Button
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var lay: LinearLayout
    lateinit var lay1: LinearLayout
    lateinit var lay2: LinearLayout
    lateinit var lay3: LinearLayout
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        one1 = findViewById(R.id.one1)
        two1 = findViewById(R.id.two1)
        three = findViewById(R.id.three)
        received = findViewById(R.id.received)
        work_underway = findViewById(R.id.work_underway)
        received1 = findViewById(R.id.received1)
        work_underway1 = findViewById(R.id.work_underway1)
        back_to_main = findViewById(R.id.back_to_main)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        lay = findViewById(R.id.lay)
        lay1 = findViewById(R.id.lay1)
        lay2 = findViewById(R.id.lay2)
        lay3 = findViewById(R.id.lay3)
        three1 = findViewById(R.id.three1)
        title.text = getString(R.string.follow_up_order)
        back.setOnClickListener { onBackPressed()
            finish()
        }
        back_to_main.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        Track(null,null)
        received.setOnClickListener { Track("received_order_client",1) }
        work_underway.setOnClickListener { Track("delivery_order_provider",1) }
        received1.setOnClickListener { Track("received_order_provider",1) }
        work_underway1.setOnClickListener { Track("delivery_order_client",1) }

    }

    fun Track(status:String?,confirm:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Track(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
            id,status,confirm)?.enqueue(object : Callback<TrackOrderDelegateResponse> {
            override fun onFailure(call: Call<TrackOrderDelegateResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onResponse(
                call: Call<TrackOrderDelegateResponse>,
                response: Response<TrackOrderDelegateResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Log.e("response", Gson().toJson(response.body()?.data))
                        if (response.body()?.data?.delegate_status.equals("accepted")){
                            if (response.body()?.data?.provider_status.equals("accepted")){
                                lay.visibility = View.VISIBLE
                                lay1.visibility = View.GONE
                                lay2.visibility = View.VISIBLE
                                lay3.visibility = View.GONE
                                received.visibility = View.VISIBLE
                                work_underway.visibility = View.GONE
                                back_to_main.visibility = View.GONE
                                one.setImageResource(R.mipmap.right)
                                one.background = mContext.getDrawable(R.drawable.green_circle)
                                two.setImageResource(R.mipmap.right)
                                two.background = mContext.getDrawable(R.drawable.white_green_circle)
                                three.setImageResource(R.mipmap.right)
                                three.background = mContext.getDrawable(R.drawable.white_green_circle)
                            }else if (response.body()?.data?.provider_status.equals("finished_order_provider")){
                                lay.visibility = View.GONE
                                lay1.visibility = View.VISIBLE
                                lay2.visibility = View.GONE
                                lay3.visibility = View.VISIBLE
                                received.visibility = View.GONE
                                work_underway.visibility = View.GONE
                                back_to_main.visibility = View.GONE
                                work_underway1.visibility = View.GONE
                                received1.visibility = View.VISIBLE
                                one1.setImageResource(R.mipmap.right)
                                one1.background = mContext.getDrawable(R.drawable.green_circle)
                                two1.setImageResource(R.mipmap.right)
                                two1.background = mContext.getDrawable(R.drawable.white_green_circle)
                                three1.setImageResource(R.mipmap.right)
                                three1.background = mContext.getDrawable(R.drawable.white_green_circle)

                            }

                        }else if (response.body()?.data?.delegate_status.equals("received_order_client")){
                            if (response.body()?.data?.provider_status.equals("received_order_client")) {
                                lay.visibility = View.VISIBLE
                                lay1.visibility = View.GONE
                                lay2.visibility = View.VISIBLE
                                lay3.visibility = View.GONE
                                received.visibility = View.GONE
                                work_underway.visibility = View.VISIBLE
                                back_to_main.visibility = View.GONE
                                one.setImageResource(R.mipmap.right)
                                one.background = mContext.getDrawable(R.drawable.green_circle)
                                two.setImageResource(R.mipmap.right)
                                two.background = mContext.getDrawable(R.drawable.green_circle)
                                three.setImageResource(R.mipmap.right)
                                three.background = mContext.getDrawable(R.drawable.white_green_circle)
                            }

                        }else if (response.body()?.data?.delegate_status.equals("delivery_order_provider")){
                            if (response.body()?.data?.provider_status.equals("received_order_client")) {
                                lay.visibility = View.VISIBLE
                                lay1.visibility = View.GONE
                                lay2.visibility = View.VISIBLE
                                lay3.visibility = View.GONE
                                received.visibility = View.GONE
                                work_underway.visibility = View.GONE
                                back_to_main.visibility = View.VISIBLE
                                one.setImageResource(R.mipmap.right)
                                one.background = mContext.getDrawable(R.drawable.green_circle)
                                two.setImageResource(R.mipmap.right)
                                two.background = mContext.getDrawable(R.drawable.green_circle)
                                three.setImageResource(R.mipmap.right)
                                three.background = mContext.getDrawable(R.drawable.green_circle)
                            }

                        }else if (response.body()?.data?.delegate_status.equals("completed")){
                            if (response.body()?.data?.provider_status.equals("completed")){
                                lay.visibility = View.GONE
                                lay1.visibility = View.VISIBLE
                                lay2.visibility = View.GONE
                                lay3.visibility = View.VISIBLE
                                received.visibility = View.GONE
                                work_underway.visibility = View.GONE
                                back_to_main.visibility = View.VISIBLE
                                one1.setImageResource(R.mipmap.right)
                                one1.background = mContext.getDrawable(R.drawable.green_circle)
                                two1.setImageResource(R.mipmap.right)
                                two1.background = mContext.getDrawable(R.drawable.green_circle)
                                three1.setImageResource(R.mipmap.right)
                                three1.background = mContext.getDrawable(R.drawable.green_circle)

                            }
                        }else if (response.body()?.data?.delegate_status.equals("received_order_provider")){
                            if (response.body()?.data?.provider_status.equals("finished_order_provider")){
                                lay.visibility = View.GONE
                                lay1.visibility = View.VISIBLE
                                lay2.visibility = View.GONE
                                lay3.visibility = View.VISIBLE
                                received.visibility = View.GONE
                                work_underway.visibility = View.GONE
                                back_to_main.visibility = View.GONE
                                work_underway1.visibility = View.VISIBLE
                                received1.visibility = View.GONE
                                one1.setImageResource(R.mipmap.right)
                                one1.background = mContext.getDrawable(R.drawable.green_circle)
                                two1.setImageResource(R.mipmap.right)
                                two1.background = mContext.getDrawable(R.drawable.green_circle)
                                three1.setImageResource(R.mipmap.right)
                                three1.background = mContext.getDrawable(R.drawable.white_green_circle)
                            }
                        }


                        else if (response.body()?.data?.delegate_status.equals("delivery_order_client")){
                            if (response.body()?.data?.provider_status.equals("finished_order_provider")){
                                lay.visibility = View.GONE
                                lay1.visibility = View.VISIBLE
                                lay2.visibility = View.GONE
                                lay3.visibility = View.VISIBLE
                                received.visibility = View.GONE
                                work_underway.visibility = View.GONE
                                back_to_main.visibility = View.VISIBLE
                                work_underway1.visibility = View.GONE
                                received1.visibility = View.GONE
                                one1.setImageResource(R.mipmap.right)
                                one1.background = mContext.getDrawable(R.drawable.green_circle)
                                two1.setImageResource(R.mipmap.right)
                                two1.background = mContext.getDrawable(R.drawable.green_circle)
                                three1.setImageResource(R.mipmap.right)
                                three1.background = mContext.getDrawable(R.drawable.green_circle)
                            }
                        }

                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })


    }
}