package com.aait.cleanappprovider.UI.Activities.Delegate

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.text.InputType
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.BaseResponse
import com.aait.cleanappprovider.Models.UserModel
import com.aait.cleanappprovider.Models.UserResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Delegate.MainActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.aait.cleanappprovider.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_profile
    lateinit var back: ImageView
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var image: CircleImageView
    lateinit var change: ImageView
    lateinit var name: TextView
    lateinit var user_name: EditText
    lateinit var phone: EditText
    lateinit var password: TextView
    lateinit var description: EditText
    lateinit var description_en: EditText
    lateinit var confirm: Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish()}
        menu.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.profile)
        image = findViewById(R.id.image)
        change = findViewById(R.id.change)
        name = findViewById(R.id.name)
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        description = findViewById(R.id.description)
        description_en = findViewById(R.id.description_en)
        confirm = findViewById(R.id.confirm)
        getProfile()

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(user_name,getString(R.string.the_name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(description,getString(R.string.description))||
                CommonUtil.checkEditError(description_en,getString(R.string.description_en))){
                return@setOnClickListener
            }else{
                edit()
            }
        }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        change.setOnClickListener { if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        } }

        password.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password_delegate)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)
            val view = dialog?.findViewById<ImageView>(R.id.view)
            val view_pass = dialog?.findViewById<ImageView>(R.id.view1)
            val view_confirm = dialog?.findViewById<ImageView>(R.id.view2)
            val save = dialog?.findViewById<Button>(R.id.confirm)
            view?.setOnClickListener { if (old_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                old_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                old_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
            }
            view_pass?.setOnClickListener { if (new_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                new_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                new_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
            }
            view_confirm?.setOnClickListener { if (confirm_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                confirm_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                confirm_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
            }

            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                    CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                    CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                        confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                            object : Callback<BaseResponse> {
                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                    CommonUtil.handleException(mContext,t)
                                    t.printStackTrace()
                                    hideProgressDialog()
                                }

                                override fun onResponse(
                                    call: Call<BaseResponse>,
                                    response: Response<BaseResponse>
                                ) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                            dialog?.dismiss()
                                        }else{
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                                        }
                                    }
                                }
                            }
                        )
                    }
                }

            }
            dialog?.show()
        }

    }

    fun SetData(userModel: UserModel){
        Glide.with(mContext).asBitmap().load(userModel.avatar).into(image)
        name.text = userModel.name
        user_name.setText(userModel.name)
        phone.setText(userModel.phone)
        description.setText(userModel.description_ar)
        description_en.setText(userModel.description_en)
    }

    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.editDelegate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,null,null,null,null)
            ?.enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            mSharedPrefManager.userData = response.body()?.data!!
                            SetData(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun edit(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.editDelegate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,user_name.text.toString(),
            phone.text.toString(),description.text.toString(),description_en.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        startActivity(Intent(this@ProfileActivity,
                            MainActivity::class.java))
                        this@ProfileActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]

            Glide.with(mContext).load(ImageBasePath).into(image)

            if (ImageBasePath != null) {
                upLoad(ImageBasePath!!)
                //ID.text = getString(R.string.Image_attached)
            }
        }
    }

    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.EditAvatar(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,filePart)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        mSharedPrefManager.userData = response.body()?.data!!
                        SetData(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}