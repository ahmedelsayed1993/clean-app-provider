package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.OrderDetailsModel
import com.aait.cleanappprovider.Models.OrderDetailsResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PendingOrderDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_pending_order_details
    var id = 0
    lateinit var Image: ImageView

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var invoice: TextView
    lateinit var location: TextView
    lateinit var pay: TextView
    lateinit var notes: TextView
    lateinit var follow: Button
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        Image = findViewById(R.id.Image)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        invoice = findViewById(R.id.invoice)
        location = findViewById(R.id.location)
        pay = findViewById(R.id.pay)

        notes = findViewById(R.id.notes)
        follow = findViewById(R.id.follow)
        title.text = getString(R.string.booking_details)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        invoice.setOnClickListener { val intent = Intent(this,InvoiceActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)}
        follow.setOnClickListener {
            if (mSharedPrefManager.userData.category_key.equals("clothes")){
            val intent = Intent(this@PendingOrderDetailsActivity,OrderDelegatesActivity::class.java)
            intent.putExtra("order",id)
            intent.putExtra("type","abnormal")
            startActivity(intent)
            finish()
            }else{
                val intent = Intent(this@PendingOrderDetailsActivity,OtherDelegateActivity::class.java)
                intent.putExtra("order",id)
                intent.putExtra("type","abnormal")
                startActivity(intent)
                finish()
            }
            }
        getOrder()

    }

    fun setData(orderDetailsModel: OrderDetailsModel){
        Glide.with(mContext).load(orderDetailsModel.image).into(Image)
        name.text = orderDetailsModel.username
        for (i in 0..orderDetailsModel.items!!.size-1){
            if (orderDetailsModel.items!!.get(i).title.equals("address")){
                address.text = orderDetailsModel.items!!.get(i).details
                location.text = orderDetailsModel.items!!.get(i).details
            }else if (orderDetailsModel.items!!.get(i).title.equals("payment")){
                pay.text = orderDetailsModel.items!!.get(i).details
            }
            else if (orderDetailsModel.items!!.get(i).title.equals("notes")){
                notes.text = orderDetailsModel.items!!.get(i).details
            }

        }


    }

    fun getOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!,null
            ,"provider")?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}