package com.aait.cleanappprovider.UI.Activities.Delegate

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Delegate.MainActivity
import com.aait.cleanappprovider.UI.Adapters.OrdersTapAdapter
import com.aait.cleanappprovider.UI.Adapters.SubscribeDelegateTapAdapter
import com.google.android.material.tabs.TabLayout

class OrdersActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_orders
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeDelegateTapAdapter? = null
    lateinit var menu: ImageView

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish()}
        title.text = getString(R.string.orders)
        menu.setOnClickListener { onBackPressed()
            finish()}
        orders = findViewById(R.id.orders)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeDelegateTapAdapter(mContext,supportFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)

    }
}