package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ClothesServiceModel
import com.aait.cleanappprovider.Models.ServiceModel
import com.aait.cleanappprovider.R

class ServAdapter (context: Context, data: MutableList<ServiceModel>) :
    ParentRecyclerAdapter<ServiceModel>(context, data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_servce, parent, false)
        return ViewHolder(itemView)
    }
    var android.widget.TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        viewHolder.price.addTextChangedListener (object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                productModel?.price = charSequence.toString()



            }

            override fun afterTextChanged(editable: Editable) {}
        })

//        if (viewHolder.check.isChecked){
//            viewHolder.price.visibility = View.VISIBLE
//            productModel.selected = true
//            // notifyDataSetChanged()
//        }else{
//            viewHolder.price.visibility = View.GONE
//            productModel.selected = false
//            // notifyDataSetChanged()
//        }










    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var price=itemView.findViewById<TextView>(R.id.service4)


//        internal fun ViewHolder(itemView: View) {
//
//            setClickableRootView(itemView)
////            addServiceAdapter = AddServiceAdapter(mcontext)
//            services.setLayoutManager(
//                LinearLayoutManager(
//                    mcontext,
//                    LinearLayoutManager.HORIZONTAL,
//                    false
//                )
//            )
//
//            services.adapter = addServiceAdapter
//        }



    }
}