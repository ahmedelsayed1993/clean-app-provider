package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.ListModel
import com.aait.cleanappprovider.Models.SubCategoryModel
import com.aait.cleanappprovider.Models.SubCategoryResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R

import com.aait.cleanappprovider.UI.Adapters.ServicesAdapter
import com.aait.cleanappprovider.UI.Adapters.SubCatsAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubCategoryActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.selected){
            subCatsAdapter.notifyDataSetChanged()
            if (categories.get(position).selected!!){
                categories.get(position).selected = false

                category = 0
                name = ""
                Log.e("id", category.toString())
                subCatsAdapter.notifyDataSetChanged()
            }else {
                categories.get(position).selected = true

                category = categories.get(position).id!!
                name = categories.get(position).name!!
                subCatsAdapter.selected = position
                Log.e("id", category.toString())
                subCatsAdapter.notifyDataSetChanged()
            }
        }else if (view.id == R.id.service_lay){

            if (ServicesModel.get(position).checked==0){
                ServicesModel.get(position).checked = 1
                ServicesModel1.add(ListModel(ServicesModel.get(position)?.id,ServicesModel.get(position)?.name))
                servicesAdapter.notifyDataSetChanged()
                Log.e("list",Gson().toJson(ServicesModel1))
            }else{
                ServicesModel.get(position).checked = 0
                Log.e("position",position.toString())
                if (ServicesModel1.size>1) {
                    for (i in 0..ServicesModel1.size - 1){
                        Log.e("rrr",ServicesModel1.get(i).id.toString())
                        Log.e("ttt",ServicesModel.get(position)?.id.toString())
                        if (ServicesModel1.get(i).id==ServicesModel.get(position)?.id){
                            var list = ListModel(ServicesModel1.get(i).id,ServicesModel1.get(i).name)
                            ServicesModel1.removeAt(
                                i
                            )
                            break
                        }
                    }

                }else if (ServicesModel1.size==1){
                    ServicesModel1.clear()
                }

                servicesAdapter.notifyDataSetChanged()
                Log.e("list1",Gson().toJson(ServicesModel1))
            }
        }
        else if (view.id == R.id.check){

            if (ServicesModel.get(position).checked==0){
                ServicesModel.get(position).checked = 1
                ServicesModel1.add(ListModel(ServicesModel.get(position)?.id,ServicesModel.get(position)?.name))
                servicesAdapter.notifyDataSetChanged()
                Log.e("list",Gson().toJson(ServicesModel1))
            }else{
                ServicesModel.get(position).checked = 0
                Log.e("position",position.toString())
                if (ServicesModel1.size>1) {
                    for (i in 0..ServicesModel1.size - 1){
                        Log.e("rrr",ServicesModel1.get(i).id.toString())
                        Log.e("ttt",ServicesModel.get(position)?.id.toString())
                        if (ServicesModel1.get(i).id==ServicesModel.get(position)?.id){
                            var list = ListModel(ServicesModel1.get(i).id,ServicesModel1.get(i).name)
                            ServicesModel1.removeAt(
                                i
                            )
                            break
                        }
                    }

                }else if (ServicesModel1.size==1){
                    ServicesModel1.clear()
                }

                servicesAdapter.notifyDataSetChanged()
                Log.e("list1",Gson().toJson(ServicesModel1))
            }
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_subcats

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var cats:RecyclerView
    lateinit var services:RecyclerView
    var categories = ArrayList<SubCategoryModel>()
    var ServicesModel = ArrayList<ListModel>()
    var ServicesModel1 = ArrayList<ListModel>()
    lateinit var linearLayoutManager:LinearLayoutManager
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var subCatsAdapter: SubCatsAdapter
    lateinit var servicesAdapter: ServicesAdapter
    lateinit var confirm:Button
    var user = 0
    var category_id = ""
    var category_name = ""
    lateinit var subCategoryModel : SubCategoryModel
    var category = 0
    var name = ""

    override fun initializeComponents() {
        user = intent.getIntExtra("user" ,0)
        category_id = intent.getStringExtra("category_id")
        category_name = intent.getStringExtra("category_name")

        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        cats = findViewById(R.id.cats)
        services = findViewById(R.id.services)
        confirm = findViewById(R.id.confirm)
        title.text = category_name
        back.setOnClickListener { onBackPressed()
        this@SubCategoryActivity.finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        gridLayoutManager = GridLayoutManager(mContext,2)
        subCatsAdapter = SubCatsAdapter(mContext,categories,R.layout.subcategory_card)
        servicesAdapter = ServicesAdapter(mContext,ServicesModel,R.layout.services_cart)
        servicesAdapter.setOnItemClickListener(this)
        cats.layoutManager = gridLayoutManager
        services.layoutManager = linearLayoutManager
        subCatsAdapter.setOnItemClickListener(this)
        cats.adapter = subCatsAdapter
        services.adapter = servicesAdapter
        getSubCategories(user,category_id.toInt())

        confirm.setOnClickListener {
            if (category==0){
                CommonUtil.makeToast(mContext,getString(R.string.choose_category))

            }else{
                if (ServicesModel1.isEmpty()){
                    CommonUtil.makeToast(mContext,getString(R.string.Choose_the_service_provided))
                }else {
                    val intent = Intent(this, ProductsActvity::class.java)
                    intent.putExtra("category", category)
                    intent.putExtra("cat_name", name)
                    intent.putExtra("services", ServicesModel1)
                    intent.putExtra("user", user)
                    startActivity(intent)
                }
            }
        }

    }
    fun getSubCategories(user_id:Int,cat:Int){
        showProgressDialog(mContext.getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.subCategory(mLanguagePrefManager.appLanguage,user_id,cat)?.enqueue(object :
            Callback<SubCategoryResponse>{
            override fun onFailure(call: Call<SubCategoryResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoryResponse>,
                response: Response<SubCategoryResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        servicesAdapter.updateAll(response.body()?.services!!)
                        subCatsAdapter.updateAll(response.body()?.data!!)
                        ServicesModel1.takeLast(response.body()?.services?.size!!)
                        category = categories.get(0).id!!
                        name = categories.get(0).name!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}