package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.ProductServicesAdapter
import com.aait.cleanappprovider.UI.Adapters.ServiceAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductsActvity:Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.check){
            productServicesAdapter.notifyItemChanged(position)




        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_products
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var name:TextView
    lateinit var services:RecyclerView
    lateinit var products:RecyclerView
    lateinit var confirm:Button
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var serviceAdapter: ServiceAdapter
    lateinit var productServicesAdapter: ProductServicesAdapter
    var productModels= ArrayList<ProductModel>()
    var productModels1 = ArrayList<ClothesModel>()
    var listModel = ArrayList<ListModel>()
    var category = 0
    var user = 0
    var cat_name = ""
    var servicesModel = ArrayList<Services>()
    var servicesModel1 = ArrayList<Services>()
    lateinit var productModel: ProductModel
    var Services = ArrayList<ListModel>()
    var ServiceModels = ArrayList<ServiceModel>()
    var ids = ArrayList<Int>()


    override fun initializeComponents() {
        category = intent.getIntExtra("category",0)
        user = intent.getIntExtra("user",0)
        Services = intent.getSerializableExtra("services") as ArrayList<ListModel>
        cat_name = intent.getStringExtra("cat_name")
        Log.e("ser",Gson().toJson(Services))
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        services = findViewById(R.id.services)
        products = findViewById(R.id.products)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        for (i in 0..Services.size-1){
            ServiceModels.add(ServiceModel(Services.get(i).id,""))
            ids.add(Services.get(i).id!!)
        }
        Log.e("prices",Gson().toJson(ServiceModels))
        Log.e("ids",ids.joinToString().replace(" ",""))
        title.text = cat_name
        gridLayoutManager = GridLayoutManager(mContext,Services.size)

        serviceAdapter = ServiceAdapter(mContext,listModel,R.layout.recycler_services)

        services.layoutManager = gridLayoutManager
        services.adapter = serviceAdapter
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        productServicesAdapter = ProductServicesAdapter(mContext,
            productModels,R.layout.recycler_clothes)
        productServicesAdapter.setOnItemClickListener(this@ProductsActvity)
        products.layoutManager = linearLayoutManager
        products.adapter = productServicesAdapter
        serviceAdapter.updateAll(Services)
        getProduct()
        Log.e("adapter",Gson().toJson(serviceAdapter.getData()))
        confirm.setOnClickListener {
            productModels1.clear()
            servicesModel1.clear()

            Log.e("llll",Gson().toJson(productServicesAdapter.data))

            for (i in 0..productServicesAdapter.data.size-1) {
                servicesModel1.clear()
                servicesModel.clear()
                if (productServicesAdapter.data.get(i).selected) {
                             productModel = productServicesAdapter.data.get(i)
                    for (j in 0..productModel.services?.size!! - 1) {

                        if (productModel.services?.get(j)?.price!!.equals(
                                ""
                            )
                        ) {

                        } else {
                            servicesModel.add(
                                Services(
                                    productModel.services?.get(
                                        j
                                    )?.service_id,
                                    productModel.services?.get(j)?.price
                                )

                            )
                            Log.e("serv", Gson().toJson(servicesModel))
                        }
                    }
                    Log.e("services", Gson().toJson(servicesModel))
                    var service = Gson().toJson(servicesModel)
                    var ser = service.replace("\\","")
                    for (i in 0..servicesModel.size-1){
                        servicesModel1.add(Services(servicesModel.get(i).service_id,servicesModel.get(i).price,servicesModel.get(i).subcategory_id))
                    }

                    var colthesModel = ClothesModel(
                        productServicesAdapter.data.get(i).id,
                        category.toString(),
                        servicesModel1
                    )

                    Log.e("clothes!!!!!", Gson().toJson(colthesModel))
                    if (colthesModel?.services?.isEmpty()!!) {

                    } else {
                        productModels1.add(colthesModel)
                    }
                }else{

                }


                Log.e("product", Gson().toJson(productModels))
                Log.e("product1", Gson().toJson(productModels1))
                if (productModels1?.isEmpty()) {
                    CommonUtil.makeToast(mContext,getString(R.string.enter_prices))
                } else {
                     addProducts()
                }

            }



            //productServicesAdapter.notifyDataSetChanged()

        }

    }
    fun getProduct(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProducts(mLanguagePrefManager.appLanguage,user,category,ids.joinToString().replace(" ",""))?.enqueue(object :
            Callback<ProductResponse>{
            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        productServicesAdapter.updateAll(response.body()?.data!!)
                        Log.e("products",Gson().toJson(response.body()?.data!!))
                        productModels = response.body()?.data!!
                        productModels = productServicesAdapter.data as ArrayList<ProductModel>
                        Log.e("rrrrr",Gson().toJson(productModels))
                        name.text = response.body()?.username
                    }
                }
            }
        })
    }

    fun addProducts(){
        showProgressDialog(getString(R.string.please_wait))
        Log.e("gson",Gson().toJson(productModels1))
        Client.getClient()?.create(Service::class.java)?.addProduct(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData?.id!!,Gson().toJson(productModels1),null)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@ProductsActvity, ClothesServicesActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        }

        )
    }
}


