package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.*
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.AirAdapter
import com.aait.cleanappprovider.UI.Adapters.CarsAdapter
import com.aait.cleanappprovider.UI.Adapters.LocationsAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationsActivity:Parent_Activity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_locations

    lateinit var sizes: RecyclerView

    lateinit var confirm: Button
    lateinit var carsAdapter: CarsAdapter
    var productModels = ArrayList<ProductModel>()
    lateinit var airAdapter: AirAdapter
    var productModels1 = ArrayList<ProductModel>()
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var gridLayoutManager1: GridLayoutManager
    var airModel = ArrayList<CarModel>()
    var cars = ArrayList<CarModel>()
    var products = ArrayList<CarModel>()
    var user = 0
    var category_id = ""
    var category_name = ""
    var subcategory = ""
    var cats = ""
    lateinit var title:TextView
    lateinit var back: ImageView
    lateinit var locationsAdapter: LocationsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var subCategoryModels = ArrayList<SubCategoryModel>()



    override fun initializeComponents() {
        user = intent.getIntExtra("user" ,0)
        category_id = intent.getStringExtra("category_id")
        category_name = intent.getStringExtra("category_name")
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.visibility = View.GONE
        title.text = category_name

        sizes = findViewById(R.id.sizes)

        confirm = findViewById(R.id.confirm)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        locationsAdapter = LocationsAdapter(mContext,subCategoryModels,R.layout.recycler_locations)
      //  locationsAdapter.setOnItemClickListener(this)
//        carsAdapter = CarsAdapter(mContext,productModels,R.layout.recycler_cars)
//        airAdapter = AirAdapter(mContext,productModels1,R.layout.recycler_air)
//        carsAdapter.setOnItemClickListener(this)
//        airAdapter.setOnItemClickListener(this)
//        gridLayoutManager = GridLayoutManager(mContext,2)
//        gridLayoutManager1 = GridLayoutManager(mContext,2)
        sizes.layoutManager = linearLayoutManager
        sizes.adapter = locationsAdapter
//        services.layoutManager = gridLayoutManager1
//        services.adapter = airAdapter
        getSubCategories(user,category_id.toInt())
        confirm.setOnClickListener {
            cars.clear()
            airModel.clear()
           // products.clear()
            locationsAdapter.notifyDataSetChanged()
            for (i in 0..locationsAdapter.getData().size - 1){
                for(x in 0..locationsAdapter.getData().get(i)?.products!!.size - 1) {
                    if (!locationsAdapter.getData().get(i)?.products?.get(x)?.selected!!) {
                        for (j in 0..cars.size - 1) {
                            if (cars.get(j).product!!.equals(locationsAdapter.getData().get(i)?.products?.get(x)?.id.toString())) {
                                cars.removeAt(j)
                            }
                        }

                    } else {
                        if (locationsAdapter.getData().get(i)?.products?.get(x)?.price.equals("")) {
                            CommonUtil.makeToast(mContext, getString(R.string.enter_price))
                        } else {
                            cars.add(
                                CarModel(
                                    locationsAdapter.getData().get(i)?.products?.get(x)?.id.toString(),
                                    locationsAdapter.getData().get(i)?.products?.get(x)?.price,
                                    subcategory
                                )
                            )
                        }
                    }
                }
            }
            Log.e("cars", Gson().toJson(cars))
            if (!cars.isEmpty()){
                for (i in 0..cars.size-1){
                    products.add(CarModel(cars.get(i).product,cars.get(i).price,cars.get(i).subcategory_id))
                }
            }
            Log.e("products",Gson().toJson(products))
//            for (i in 0..airAdapter.getData().size - 1){
//                if (!airAdapter.getData().get(i).selected!!){
//                    for (j in 0..airModel.size-1){
//                        if (airModel.get(j).product!!.equals(airAdapter.getData().get(i).id.toString())){
//                            airModel.removeAt(j)
//                        }
//                    }
//                }else{
//                    if (airAdapter.getData().get(i).price.equals("")){
//                        CommonUtil.makeToast(mContext,getString(R.string.enter_price))
//                    }else {
//                        airModel.add(
//                            CarModel(
//                                airAdapter.getData().get(i).id.toString(),
//                                airAdapter.getData().get(i).price, cats
//                            )
//                        )
//                    }
//
//                }
//            }
            Log.e("services", Gson().toJson(airModel))
            if (!airModel.isEmpty()){
                for (i in 0..airModel.size-1){
                    products.add(CarModel(airModel.get(i).product,airModel.get(i).price,airModel.get(i).subcategory_id))
                }
            }
            Log.e("all",Gson().toJson(products))
            if (products.isEmpty()){
               CommonUtil.makeToast(mContext,getString(R.string.choose_cat))
            }else{
                if (products.get(0).price.equals("")){
                    CommonUtil.makeToast(mContext,getString(R.string.enter_price))
                }else {
                    addProducts()
                }
            }
        }

    }
    fun getSubCategories(user_id:Int,cat:Int){
        showProgressDialog(mContext.getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.subCategory(mLanguagePrefManager.appLanguage,user_id,cat)?.enqueue(object :
            Callback<SubCategoryResponse> {
            override fun onFailure(call: Call<SubCategoryResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoryResponse>,
                response: Response<SubCategoryResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        // servicesAdapter.updateAll(response.body()?.services!!)
                        subcategory = response.body()?.data?.get(0)?.id.toString()
                        cats = response.body()?.data?.get(1)?.id.toString()
//                        size.text = response.body()?.data?.get(0)?.name
//                        air.text = response.body()?.data?.get(1)?.name
                        locationsAdapter.updateAll(response.body()?.data!!)
//                        airAdapter.updateAll(response.body()?.data?.get(1)?.products!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {


    }

    fun addProducts(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.addProduct(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData?.id!!,Gson().toJson(products),null)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@LocationsActivity,ServicesActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        }

        )
    }
}