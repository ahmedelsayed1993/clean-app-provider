package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.widget.Button
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R

class DelegateDoneActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_done
    lateinit var back:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java) )
        finish()}
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java) )
        finish()
    }
}