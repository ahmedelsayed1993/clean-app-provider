package com.aait.cleanappprovider.UI.Adapters

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.cleanappprovider.UI.Fragments.FreeFragment
import com.aait.cleanappprovider.UI.Fragments.PrivateFragment
import com.aait.cleanappprovider.UI.Fragments.RandomFragment

import com.aait.cleanappprovider.R



class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager,internal var id:Int,internal var type:String
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            PrivateFragment.newInstance(id,type)
        } else if (position == 1){
            FreeFragment.newInstance(id,type)
        }else{
            RandomFragment.newInstance(id,type)
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.private_delegate)
        } else if (position == 1) {
            context.getString(R.string.free_delegate)
        }else{
            context.getString(R.string.random_delegate)
        }
    }
}
