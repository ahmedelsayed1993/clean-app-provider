package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.view.View
import android.widget.*
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.SwitchNotification
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.SplashActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_settings

    lateinit var menu: ImageView
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ToggleButton
    lateinit var lang: LinearLayout
    lateinit var lang_lay: RadioGroup
    lateinit var english: RadioButton
    lateinit var arabic: RadioButton

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        lang = findViewById(R.id.lang)
        lang_lay = findViewById(R.id.lang_lay)
        english = findViewById(R.id.english)
        arabic = findViewById(R.id.arabic)
        menu.setOnClickListener { onBackPressed()
            finish()}
        back.setOnClickListener { startActivity(Intent(this@SettingsActivity,MainActivity::class.java))
            finish()}
        switch()
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            arabic.isChecked = true
        }else{
            english.isChecked = true
        }
        lang.setOnClickListener {
            if (lang_lay.visibility == View.VISIBLE){
                lang_lay.visibility = View.GONE
            }else if (lang_lay.visibility == View.GONE){
                lang_lay.visibility = View.VISIBLE
            }
        }
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
//            val intent = Intent(applicationContext, SplashActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            intent.putExtra("EXIT", true)
//            startActivity(intent)
//            if (getIntent().getBooleanExtra("EXIT", false)) {
//                finish()
//            }
            finishAffinity()
            startActivity(Intent(this@SettingsActivity, SplashActivity::class.java))
        }
        english.setOnClickListener {
            mLanguagePrefManager.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this@SettingsActivity,SplashActivity::class.java))
        }
        notification.setOnClickListener {
            if (notification.isChecked){
                switch(1)
            }else{
                switch(0)
            }
        }

    }

    fun switch(state:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.switch(mSharedPrefManager.userData.id!!,state)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@SettingsActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun switch(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.switch(mSharedPrefManager.userData.id!!,null)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            notification.isChecked = true
                        }else{
                            notification.isChecked = false
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}