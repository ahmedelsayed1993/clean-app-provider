package com.aait.cleanappprovider.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.GPS.GPSTracker
import com.aait.cleanappprovider.GPS.GpsTrakerListener
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Provider.LoginActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.aait.cleanappprovider.Uitls.DialogUtil
import com.aait.cleanappprovider.Uitls.PermissionUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import java.io.IOException
import java.util.*

class PreLoginActivity:Parent_Activity(),GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
               // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_pre_login
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""


    private var mAlertDialog: AlertDialog? = null
    lateinit var provider:Button
    lateinit var delegate:Button
    lateinit var english:Button
    lateinit var arabic:Button

    override fun initializeComponents() {
        provider = findViewById(R.id.provider)
        delegate = findViewById(R.id.delegate)
        english = findViewById(R.id.english)
        arabic = findViewById(R.id.arabic)
        getLocationWithPermission()
        provider.setOnClickListener { startActivity(Intent(this@PreLoginActivity,LoginActivity::class.java)) }
        delegate.setOnClickListener { startActivity(Intent(this@PreLoginActivity,com.aait.cleanappprovider.UI.Activities.Delegate.LoginActivity::class.java)) }
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this@PreLoginActivity, SplashActivity::class.java))
        }
        english.setOnClickListener {
            mLanguagePrefManager.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this@PreLoginActivity,SplashActivity::class.java))
        }
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
               // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
//                        Toast.makeText(
//                            mContext,
//                            resources.getString(R.string.detect_location),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                       // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

               // googleMap.clear()
               // putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}