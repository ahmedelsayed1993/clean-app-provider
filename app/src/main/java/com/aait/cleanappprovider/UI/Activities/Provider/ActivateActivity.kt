package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.AboutAppResponse
import com.aait.cleanappprovider.Models.UserModel
import com.aait.cleanappprovider.Models.UserResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_activate_provider
    lateinit var one: EditText
    lateinit var two: EditText
    lateinit var three: EditText
    lateinit var four: EditText
    lateinit var next: Button
    lateinit var resend: LinearLayout
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        next = findViewById(R.id.next)
        resend = findViewById(R.id.resend)
        one.setSelection(0)
        two.setSelection(0)
        three.setSelection(0)
        four.setSelection(0)
        one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                two.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                three.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                four.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        next.setOnClickListener {
            if (CommonUtil.checkEditError(one,getString(R.string.activation_code))||
                CommonUtil.checkEditError(two,getString(R.string.activation_code))||
                CommonUtil.checkEditError(three,getString(R.string.activation_code))||
                CommonUtil.checkEditError(four,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                var code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
                Activate(code)

            }
        }
        resend.setOnClickListener { Resend() }

    }

    fun Activate(code:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode(userModel.id!!,code,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@ActivateActivity,CategoriesActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        this@ActivateActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ReSend(userModel.id!!)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}