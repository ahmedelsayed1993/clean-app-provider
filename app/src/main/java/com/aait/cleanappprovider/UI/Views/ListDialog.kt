package com.aait.cleanappprovider.UI.Views

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.ListModel

import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.ListAdapter


class ListDialog(
    internal var mContext: Context,
    internal var onItemClickListener: OnItemClickListener,
    internal var mCarsList: ArrayList<ListModel>,
    internal var title: String
) : Dialog(mContext) {


    internal var rvRecycle: RecyclerView? = null
    internal  var iv_close:ImageView?=null


    internal var lay_no_data: LinearLayout? = null


    internal var tv_title: TextView? = null

    internal lateinit var mLinearLayoutManager: LinearLayoutManager

    internal var mCatsList: List<ListModel>? = null

    internal lateinit var mListAdapter: ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dailog_custom_layout)
        window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window!!.setGravity(Gravity.CENTER)
        setCancelable(false)

        initializeComponents()
    }
//    override fun onCreate(savedInstanceState: Bundle) {
//        super.onCreate(savedInstanceState)
//
//
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        setContentView(R.layout.dailog_custom_layout)
//        window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//        window!!.setGravity(Gravity.CENTER)
//        setCancelable(false)
//
//        initializeComponents()
//    }

    private fun initializeComponents() {
        tv_title = findViewById<TextView>(R.id.tv_title)
        rvRecycle = findViewById(R.id.rv_recycle)
        lay_no_data = findViewById(R.id.lay_no_data)
        iv_close = findViewById(R.id.iv_close)
        tv_title!!.text = title
        mLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvRecycle!!.layoutManager = mLinearLayoutManager
        mListAdapter = ListAdapter(mContext, mCarsList,R.layout.recycler_list)
        mListAdapter.setOnItemClickListener(onItemClickListener)
        rvRecycle!!.adapter = mListAdapter

        if (mCarsList.size == 0) {
            lay_no_data!!.visibility = View.VISIBLE
        }
        iv_close?.setOnClickListener { dismiss() }

    }


}

