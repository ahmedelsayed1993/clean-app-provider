package com.aait.cleanappprovider.UI.Fragments

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanappprovider.Base.BaseFragment
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Listeners.OnItemClickListener
import com.aait.cleanappprovider.Models.OrderDelegateModel
import com.aait.cleanappprovider.Models.OrderDelegateResponse
import com.aait.cleanappprovider.Models.OrderModel
import com.aait.cleanappprovider.Models.OrderResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Delegate.FollowOrderDetailsActivity
import com.aait.cleanappprovider.UI.Activities.Provider.OrderFollowDetailsActivity
import com.aait.cleanappprovider.UI.Adapters.NewOrdersAdapter
import com.aait.cleanappprovider.UI.Adapters.OrderDelegateAdapter
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DelegateCurrentFragment: BaseFragment(), OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, FollowOrderDetailsActivity::class.java)
        intent.putExtra("id",orderDelegateModels.get(position).id)
        intent.putExtra("order_id",orderDelegateModels.get(position).order_id)
        startActivity(intent)
    }

    override val layoutResource: Int
        get() = R.layout.fragment_order_delegate

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var orderDelegateModels = java.util.ArrayList<OrderDelegateModel>()
    internal lateinit var orderDelegateAdapter: OrderDelegateAdapter

    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        orderDelegateAdapter =  OrderDelegateAdapter(mContext!!,orderDelegateModels, R.layout.recycler_delegate_order)
        orderDelegateAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = orderDelegateAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.DelegateOrders(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"accepted")?.enqueue(object :
            Callback<OrderDelegateResponse> {
            override fun onResponse(call: Call<OrderDelegateResponse>, response: Response<OrderDelegateResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                orderDelegateAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<OrderDelegateResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })
    }
}