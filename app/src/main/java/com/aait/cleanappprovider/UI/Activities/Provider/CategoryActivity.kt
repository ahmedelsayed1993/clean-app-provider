package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.widget.Button
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Models.UserModel
import com.aait.cleanappprovider.R

class CategoryActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_provider_category
    lateinit var category:Button
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        category = findViewById(R.id.category)
        category.text = mSharedPrefManager.userData.category_name
        category.setOnClickListener {
            if (mSharedPrefManager.userData.category_key.equals("clothes")) {
                val intent = Intent(this@CategoryActivity, SubCategoryActivity::class.java)
                intent.putExtra("user", mSharedPrefManager.userData.id!!)
                intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
                intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
                startActivity(intent)
                finish()
            }else if (mSharedPrefManager.userData.category_key.equals("cars")){
                val intent = Intent(this@CategoryActivity, CarsActivity::class.java)
                intent.putExtra("user", mSharedPrefManager.userData.id!!)
                intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
                intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
                startActivity(intent)
                finish()
            }else{
                val intent = Intent(this@CategoryActivity, LocationsActivity::class.java)
                intent.putExtra("user", mSharedPrefManager.userData.id!!)
                intent.putExtra("category_id", mSharedPrefManager.userData.provider_category!!)
                intent.putExtra("category_name", mSharedPrefManager.userData.category_name!!)
                startActivity(intent)
                finish()
            }
        }

    }
}