package com.aait.cleanappprovider.UI.Activities.Delegate

import android.content.Intent
import android.text.InputType
import android.widget.*
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.UserResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Delegate.AcivateActivity
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_login
    lateinit var register: LinearLayout
    lateinit var forgot: TextView
    lateinit var login:Button
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var view: ImageView
    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        register = findViewById(R.id.register)
        forgot = findViewById(R.id.forgot)
        login = findViewById(R.id.login)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        register.setOnClickListener { startActivity(Intent(this, RegisterActivity::class.java)) }
        forgot.setOnClickListener { startActivity(Intent(this, ForgotPasswordActivity::class.java)) }
        login.setOnClickListener { if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
            CommonUtil.checkEditError(password,getString(R.string.password))){
            return@setOnClickListener
        }else {
            login()
        } }
        view.setOnClickListener {
            if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }
    }

    fun login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(phone.text.toString(),password.text.toString(),deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type.equals("delegate")){
                            mSharedPrefManager.userData = response.body()?.data!!
                            mSharedPrefManager.loginStatus = true
                            startActivity(Intent(this@LoginActivity,MainActivity::class.java))
                            this@LoginActivity.finish()
                        }else{
                            CommonUtil.makeToast(mContext,getString(R.string.not_delegate))
                        }
                    }else if (response.body()?.value.equals("2")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent = Intent(this@LoginActivity, AcivateActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        this@LoginActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}