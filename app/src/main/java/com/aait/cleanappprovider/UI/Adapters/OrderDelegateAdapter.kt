package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.OrderDelegateModel
import com.aait.cleanappprovider.Models.OrderModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class OrderDelegateAdapter (context: Context, data: MutableList<OrderDelegateModel>, layoutId: Int) :
    ParentRecyclerAdapter<OrderDelegateModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.provider_name)
        viewHolder.address!!.text = listModel.address!!
        //  Glide.with(mcontext).load(listModel.image!!).into(viewHolder.photo)
        Glide.with(mcontext).load(listModel.image).into(viewHolder.photo)
        viewHolder.order_num.text = listModel.order_id!!.toString()
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var photo=itemView.findViewById<ImageView>(R.id.Image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var order_num = itemView.findViewById<TextView>(R.id.order_num)



    }
}