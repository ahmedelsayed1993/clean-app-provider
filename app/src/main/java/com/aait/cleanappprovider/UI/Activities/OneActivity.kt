package com.aait.cleanappprovider.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R

class OneActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_one
    lateinit var back:ImageView
    lateinit var skip:TextView
    lateinit var next:ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        skip = findViewById(R.id.skip)
        next = findViewById(R.id.next)
        back.setOnClickListener { onBackPressed() }
        skip.setOnClickListener {  startActivity(Intent(this@OneActivity,PreLoginActivity::class.java))}
        next.setOnClickListener { startActivity(Intent(this@OneActivity,TwoActivity::class.java)) }
    }
}