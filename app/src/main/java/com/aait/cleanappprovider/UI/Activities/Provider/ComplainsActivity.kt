package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.net.Uri
import android.provider.ContactsContract
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.ContactResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import kotlinx.android.synthetic.main.activity_provider_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComplainsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_complains_provider
    lateinit var title:TextView
    lateinit var face:ImageView
    lateinit var twitter:ImageView
    lateinit var insta:ImageView
    lateinit var name:EditText
    lateinit var mail:EditText
    lateinit var message:EditText
    lateinit var menu:ImageView
    lateinit var back:ImageView
    var facebook = ""
    var twit = ""
    var instagram = ""
    lateinit var send:Button

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        title.text = getString(R.string.contact_us)
        face = findViewById(R.id.face)
        twitter = findViewById(R.id.twitter)
        insta = findViewById(R.id.insta)
        name = findViewById(R.id.name)
        mail = findViewById(R.id.mail)
        message = findViewById(R.id.message)
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        send = findViewById(R.id.send)
        menu.setOnClickListener { onBackPressed()
        finish()}
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        getContact()
        face.setOnClickListener {
            if (!facebook.equals(""))
            {
            if (facebook.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(facebook)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$facebook"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
          }
        }
        twitter.setOnClickListener {
            if (!twit.equals(""))
            {
                if (twit.startsWith("http"))
                {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(twit)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$twit"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        insta.setOnClickListener {
            if (!instagram.equals(""))
            {
                if (instagram.startsWith("http"))
                {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(instagram)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$instagram"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }

        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.the_name))||
                    CommonUtil.checkEditError(mail,getString(R.string.mail))||
                    !CommonUtil.isEmailValid(mail,getString(R.string.correct_email))||
                    CommonUtil.checkEditError(message,getString(R.string.write_message))){
                return@setOnClickListener
            }else{
                Contact()
            }
        }


    }

    fun getContact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.contact(mLanguagePrefManager.appLanguage,null,null,null)?.enqueue(object :Callback<ContactResponse>{
            override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ContactResponse>,
                response: Response<ContactResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        facebook = response.body()?.data?.get(0)?.link!!
                        twit = response.body()?.data?.get(1)?.link!!
                       instagram = response.body()?.data?.get(2)?.link!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Contact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.contact(mLanguagePrefManager.appLanguage,name.text.toString(),mail.text.toString(),message.text.toString())?.enqueue(object :Callback<ContactResponse>{
            override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ContactResponse>,
                response: Response<ContactResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        facebook = response.body()?.data?.get(0)?.link!!
                        twit = response.body()?.data?.get(1)?.link!!
                        instagram = response.body()?.data?.get(2)?.link!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}