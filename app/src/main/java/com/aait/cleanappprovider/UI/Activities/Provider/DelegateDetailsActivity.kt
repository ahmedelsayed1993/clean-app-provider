package com.aait.cleanappprovider.UI.Activities.Provider

import android.app.Dialog
import android.content.Intent
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.widget.PopupMenu
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.Client
import com.aait.cleanappprovider.Models.AboutAppResponse
import com.aait.cleanappprovider.Models.BaseResponse
import com.aait.cleanappprovider.Models.DelegateDetailsResponse
import com.aait.cleanappprovider.Network.Service
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DelegateDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_delegate_details
    var id = 0
    lateinit var icon:ImageView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var phone:TextView
    lateinit var password:TextView
    lateinit var description:TextView
    lateinit var ratingBar: RatingBar
    lateinit var lay:LinearLayout
    lateinit var delete:TextView
    lateinit var edit:TextView

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        icon = findViewById(R.id.icon)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        description = findViewById(R.id.description)
        ratingBar = findViewById(R.id.rating)
        lay = findViewById(R.id.lay)
        delete = findViewById(R.id.delete)
        edit = findViewById(R.id.edit)
        lay.visibility = View.GONE
        lay.setOnClickListener { lay.visibility = View.GONE }
        getData()
        icon.setOnClickListener {
           lay.visibility = View.VISIBLE
        }

        edit.setOnClickListener {
            val intent = Intent(this,EditDelegateActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
            finish()
        }
        delete.setOnClickListener {val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_delete)

            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)


            confirm?.setOnClickListener {
               Delete()

            }
            cancel?.setOnClickListener { dialog?.dismiss() }
            dialog?.show()
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DelegateDetails(mLanguagePrefManager.appLanguage,id)?.enqueue(object :
            Callback<DelegateDetailsResponse>{
            override fun onFailure(call: Call<DelegateDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<DelegateDetailsResponse>,
                response: Response<DelegateDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.text = response.body()?.data?.name
                        phone.text = response.body()?.data?.phone
                        password.text = response.body()?.data?.password
                        description.text = response.body()?.data?.description
                        ratingBar.rating = response.body()?.data?.rate!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })

    }
    fun Delete(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.deleteDelegate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
        ,id)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<AboutAppResponse>, response: Response<AboutAppResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@DelegateDetailsActivity,DelegatesActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}