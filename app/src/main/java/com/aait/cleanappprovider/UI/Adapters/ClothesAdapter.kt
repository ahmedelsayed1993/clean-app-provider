package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.ClothesServiceModel
import com.aait.cleanappprovider.Models.ClothesServicesModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide
import com.google.gson.Gson

class ClothesAdapter (context: Context, data: MutableList<ClothesServicesModel>, layoutId: Int) :
    ParentRecyclerAdapter<ClothesServicesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var android.widget.TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val productModel = data.get(position)
        viewHolder.name!!.setText(productModel?.name)
        Glide.with(mcontext).asBitmap().load(productModel.image).into(viewHolder.image)
        viewHolder.clothesServicesAdapter = ClothesServicesAdapter(mcontext,viewHolder.clothesServiceModel)
        viewHolder.services.layoutManager = LinearLayoutManager(mcontext,LinearLayoutManager.HORIZONTAL,false)
        viewHolder.services.adapter = viewHolder.clothesServicesAdapter
        viewHolder.clothesServicesAdapter.updateAll(productModel.services_price!!)
        Log.e("services",Gson().toJson(viewHolder.clothesServicesAdapter.getData()))
        viewHolder.edit.setOnClickListener(View.OnClickListener { view ->onItemClickListener.onItemClick(view, position)  })
        viewHolder.delete.setOnClickListener(View.OnClickListener { view ->onItemClickListener.onItemClick(view, position)  })

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




        internal lateinit var clothesServicesAdapter:ClothesServicesAdapter
        internal var clothesServiceModel = ArrayList<ClothesServiceModel>()
        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var services = itemView.findViewById<RecyclerView>(R.id.services)
        internal var edit = itemView.findViewById<Button>(R.id.edit)
        internal var delete = itemView.findViewById<Button>(R.id.delete)

//        internal fun ViewHolder(itemView: View) {
//
//            setClickableRootView(itemView)
////            addServiceAdapter = AddServiceAdapter(mcontext)
//            services.setLayoutManager(
//                LinearLayoutManager(
//                    mcontext,
//                    LinearLayoutManager.HORIZONTAL,
//                    false
//                )
//            )
//
//            services.adapter = addServiceAdapter
//        }



    }
}