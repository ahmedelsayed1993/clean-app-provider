package com.aait.cleanappprovider.UI.Activities.Provider

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Adapters.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class OrderDelegatesActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_delegates
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var delegates: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    var id = 0
    var type = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("order",0)
        type = intent.getStringExtra("type")
        delegates = findViewById(R.id.delegates)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext,supportFragmentManager,id,type)
        ordersViewPager.setAdapter(mAdapter)
        delegates!!.setupWithViewPager(ordersViewPager)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.choose_delegate)


    }
}