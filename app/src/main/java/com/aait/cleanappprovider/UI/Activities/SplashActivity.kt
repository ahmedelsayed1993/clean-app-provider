package com.aait.cleanappprovider.UI.Activities

import android.content.Intent
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.aait.cleanappprovider.Base.Parent_Activity
import com.aait.cleanappprovider.R
import com.aait.cleanappprovider.UI.Activities.Provider.CategoriesActivity
import com.aait.cleanappprovider.UI.Activities.Provider.CategoryActivity
import com.aait.cleanappprovider.UI.Activities.Provider.MainActivity

class SplashActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false

    lateinit var logo : ImageView
    override fun initializeComponents() {
        logo = findViewById(R.id.logo)


        val logoAnimation2 = AnimationUtils.loadAnimation(this, R.anim.anim_shine)

        Handler().postDelayed({
            logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (mSharedPrefManager.loginStatus==true){
                    if (mSharedPrefManager.userData.user_type.equals("provider")){
                        if (mSharedPrefManager.userData.provider_category.equals("")){
                            val intent = Intent(this@SplashActivity, CategoriesActivity::class.java)
                            intent.putExtra("user",mSharedPrefManager.userData)
                            startActivity(intent)
                            this@SplashActivity.finish()
                        }else {
                            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                            this@SplashActivity.finish()
                        }
                    }else if (mSharedPrefManager.userData.user_type.equals("delegate")){
                        startActivity(Intent(this@SplashActivity,com.aait.cleanappprovider.UI.Activities.Delegate.MainActivity::class.java))
                        this@SplashActivity.finish()
                    }
                }else {

                    var intent = Intent(this@SplashActivity, OneActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }


}