package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.DelegateModel
import com.aait.cleanappprovider.Models.DelegateOrdersModel
import com.aait.cleanappprovider.R
import com.bumptech.glide.Glide

class DelegateOrdersAdapter (context: Context, data: MutableList<DelegateOrdersModel>, layoutId: Int) :
    ParentRecyclerAdapter<DelegateOrdersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val delegateOrdersModel = data.get(position)
        viewHolder.num!!.setText(delegateOrdersModel.order_id.toString())
        viewHolder.status!!.text = delegateOrdersModel.status!!

        viewHolder.details.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var num=itemView.findViewById<TextView>(R.id.num)
        internal var status = itemView.findViewById<TextView>(R.id.status)
        internal var details = itemView.findViewById<TextView>(R.id.details)



    }
}