package com.aait.cleanappprovider.UI.Adapters

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.cleanappprovider.Base.ParentRecyclerAdapter
import com.aait.cleanappprovider.Base.ParentRecyclerViewHolder
import com.aait.cleanappprovider.Models.DelegateOrdersModel
import com.aait.cleanappprovider.Models.ItemInvoiceModel
import com.aait.cleanappprovider.R

class InvoiceAdapter (context: Context, data: MutableList<ItemInvoiceModel>, layoutId: Int) :
    ParentRecyclerAdapter<ItemInvoiceModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val itemInvoiceModel = data.get(position)
        viewHolder.num!!.setText(itemInvoiceModel.product)
        viewHolder.service!!.text = itemInvoiceModel.services!!
        viewHolder.quentity!!.text = itemInvoiceModel.count
        viewHolder.price!!.text = itemInvoiceModel.price.toString()+" "+mcontext.getString(R.string.rs)
        if (position%2==1){
            viewHolder.lay.background = mcontext.getDrawable(R.drawable.white_shape)
        }else{
            viewHolder.lay.background = mcontext.getDrawable(R.drawable.off_white)
        }





    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var num=itemView.findViewById<TextView>(R.id.item)
        internal var service = itemView.findViewById<TextView>(R.id.service)
        internal var quentity = itemView.findViewById<TextView>(R.id.quantity)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var lay = itemView.findViewById<LinearLayout>(R.id.lay)



    }
}