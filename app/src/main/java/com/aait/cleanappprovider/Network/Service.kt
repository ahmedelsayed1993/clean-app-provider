package com.aait.cleanappprovider.Network

import com.aait.cleanappprovider.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {

    @POST("cities")
    fun cities(@Query("lang") lang:String):Call<ListResponse>

    @POST("about")
    fun About(@Query("lang") lang:String):Call<AboutAppResponse>

    @POST("terms")
    fun Terms(@Query("lang") lang:String):Call<AboutAppResponse>

    @POST("contact-us")
    fun contact(@Query("lang") lang:String,
                @Query("name") name:String?,
                @Query("email") email:String?,
                @Query("message") message:String?):Call<ContactResponse>

    @Multipart
    @POST("sign-up-provider")
    fun signUpProvider(@Query("phone") phone:String,
                       @Query("name") name:String,
                       @Query("password") password:String,
                       @Query("device_id") device_id:String,
                       @Query("device_type") device_type:String,
                       @Query("manager_name") manager_name:String,
                       @Query("owner_name") owner_name:String,
                       @Query("phone_number") phone_number:String,
                       @Query("city_id") city_id:Int,
                       @Query("address") address:String,
                       @Query("lat") lat:String,
                       @Query("lng") lng:String,
                       @Part personal_id:MultipartBody.Part,
                       @Part shop_license:MultipartBody.Part,
                       @Part commercial:MultipartBody.Part,
                       @Part bank_account:MultipartBody.Part,
                       @Query("working_from") working_from:String,
                       @Query("working_to") working_to:String,
                       @Query("workdays") workdays:String,
                       @Part place_images:List<MultipartBody.Part>,
                       @Query("lang") lang: String):Call<UserResponse>

    @Multipart
    @POST("sign-up-delegate")
    fun singUpDelegate(@Query("phone") phone:String,
                       @Query("name") name:String,
                       @Query("password") password:String,
                       @Query("device_id") device_id:String,
                       @Query("device_type") device_type:String,
                       @Query("another_phone") another_phone:String,
                       @Query("home_phone") home_phone:String,
                       @Query("home_adress") home_adress:String,
                       @Part personal_id:MultipartBody.Part,
                       @Part avatar:MultipartBody.Part,
                       @Part checkup_medical:MultipartBody.Part,
                       @Part bank_account:MultipartBody.Part,
                       @Part license_image:MultipartBody.Part,
                       @Part image_form:MultipartBody.Part,
                       @Part car_image:MultipartBody.Part,
                       @Query("car_model") car_model:String,
                       @Query("lang") lang: String):Call<UserResponse>

    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,@Query("lang") lang: String):Call<UserResponse>
    @POST("resend-code")
    fun ReSend(@Query("user_id") user_id:Int):Call<AboutAppResponse>

    @POST("sign-in")
    fun SignUp(@Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("categories")
    fun categories(@Query("lang") lang: String,
                   @Query("user_id") user_id: Int):Call<ListResponse>

    @POST("provider-category")
    fun chooseCategory(@Query("lang") lang:String,
                       @Query("user_id") user_id:Int,
                       @Query("category_id") category_id:Int):Call<ChooseCatResponse>

    @POST("subcategories")
    fun subCategory(@Query("lang") lang:String,
                    @Query("user_id") user_id: Int,
                    @Query("category_id") category_id:Int):Call<SubCategoryResponse>

    @POST("get-products")
    fun getProducts(@Query("lang") lang:String,
                    @Query("user_id") user_id: Int,
                    @Query("subcategory_id") subcategory_id:Int,
                    @Query("checked_services") checked_services:String):Call<ProductResponse>

    @POST("provider-delegates")
    fun getDelegates(@Query("lang") lang:String,
                     @Query("provider_id") provider_id:Int):Call<DelegatesResponse>

    @POST("provider-delegate-details")
    fun DelegateDetails(@Query("lang") lang: String,
                        @Query("delegate_id") delegate_id:Int):Call<DelegateDetailsResponse>
    @Multipart
    @POST("add-delegate")
    fun addDelegate(@Query("lang") lang:String,
                    @Query("provider_id") provider_id:Int,
                    @Query("name") name:String,
                    @Query("phone") phone:String,
                    @Query("description_ar") description_ar:String,
                    @Query("description_en") description_en:String,
                    @Query("password") password:String,
                    @Part avatar:MultipartBody.Part):Call<AboutAppResponse>

    @POST("products-price")
    fun addProduct(@Query("lang") lang: String,
                   @Query("provider_id") provider_id:Int,
                   @Query("prices") prices:String,
                   @Query("subcategory_price") subcategory_price:String?):Call<AboutAppResponse>

    @POST("provider-orders")
    fun orders(@Query("lang") lang:String,
               @Query("provider_id") provider_id:Int,
               @Query("status") status:String):Call<OrderResponse>

    @POST("details-order")
    fun OrderDetails(@Query("lang") lang: String,
                     @Query("order_id") order_id:Int,
                     @Query("provider_id") provider_id:Int,
                     @Query("action") action:String?,
                     @Query("user_type") user_type:String):Call<OrderDetailsResponse>

    @POST("provider-order-delegates")
    fun orderDelegats(@Query("lang") lang: String,
                      @Query("provider_id") provider_id: Int,
                      @Query("action") action:String,
                      @Query("search") search:String?):Call<DelegatesResponse>

    @POST("log-out")
    fun logOut(@Query("lang") lang: String,
               @Query("user_id") user_id: Int,
               @Query("device_type") device_type:String,
               @Query("device_id") device_id:String):Call<AboutAppResponse>

    @POST("send-order-delegate")
    fun sendDelegate(@Query("lang") lang: String,
                     @Query("provider_id") provider_id:Int,
                     @Query("delegate_id") delegate_id:Int,
                     @Query("order_id") order_id:Int,
                     @Query("delegate_type") delegate_type:String,
                     @Query("send_order") send_order:Int?):Call<SendOrderDelegateResponse>

    @POST("invoice-details")
    fun getInvoice(@Query("lang") lang: String,
                   @Query("user_id") user_id: Int,
                   @Query("order_id") order_id: Int):Call<InvoiceResponse>

    @POST("track-order-provider")
    fun followOrder(@Query("lang") lang:String,
                    @Query("provider_id") provider_id: Int,
                    @Query("order_id") order_id: Int,
                    @Query("status") status: String?,
                    @Query("confirm") confirm:Int?):Call<AboutAppResponse>

    @POST("provider-services")
    fun locationServices(@Query("lang") lang:String,
                         @Query("provider_id") provider_id:Int):Call<LocationServiceResponse>
    @POST("provider-services")
    fun carServices(@Query("lang") lang:String,
                         @Query("provider_id") provider_id:Int):Call<CarServiceResponse>

    @POST("provider-services")
    fun ClothesServices(@Query("lang") lang:String,
                        @Query("provider_id") provider_id:Int):Call<ClothesServicesResponse>

    @POST("edit-profile-provider")
    fun editProfile(@Query("lang") lang:String,
                    @Query("user_id") user_id:Int,
                    @Query("name") name:String?,
                    @Query("phone") phone: String?,
                    @Query("description_ar") description_ar: String?,
                    @Query("description_en") description_en:String?,
                    @Query("lat") lat:String?,
                    @Query("lng") lng: String?,
                    @Query("address") address:String?):Call<UserResponse>

    @Multipart
    @POST("edit-profile-provider")
    fun UpdateAvatar(@Query("lang") lang:String,
                     @Query("user_id") user_id:Int,
                     @Part avatar:MultipartBody.Part):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id: Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("notifications")
    fun Notification(@Query("lang") lang: String,
                     @Query("user_id") user_id:Int):Call<NotificationResponse>

    @POST("delete-provider-delegate")
    fun deleteDelegate(@Query("lang") lang: String,
                       @Query("provider_id") provider_id: Int,
                       @Query("delegate_id") delegate_id:Int):Call<AboutAppResponse>
    @POST("edit-provider-delegate")
    fun EditDelegate(@Query("lang") lang:String,
                     @Query("provider_id") provider_id:Int,
                     @Query("delegate_id") delegate_id:Int,
                     @Query("name") name:String,
                     @Query("description_ar") description_ar:String,
                     @Query("phone") phone:String,
                     @Query("password") password:String):Call<DelegateDetailsResponse>
    @POST("delegate-orders")
    fun DelegateOrders(@Query("lang") lang:String,
                       @Query("delegate_id") delegate_id:Int,
                       @Query("status") status:String):Call<OrderDelegateResponse>
    @POST("details-order")
    fun DetailsOrder(@Query("lang") lang:String,
                     @Query("delegate_order_id") delegate_order_id:Int,
                     @Query("order_id") order_id:Int,
                     @Query("delegate_id") delegate_id:Int,
                     @Query("action") action:String?,
                     @Query("user_type") user_type:String,
                     @Query("reason_refuse") reason_refuse:String?):Call<OrderDetailsResponse>
    @POST("track-order-delegate")
     fun Track(@Query("lang") lang:String,
               @Query("delegate_id") provider_id: Int,
               @Query("order_id") order_id: Int,
               @Query("status") status: String?,
               @Query("confirm") confirm:Int?):Call<TrackOrderDelegateResponse>

    @POST("edit-profile-provider")
    fun editDelegate(@Query("lang") lang: String,
                     @Query("user_id") user_id: Int,
                     @Query("name") name:String?,
                     @Query("phone") phone: String?,
                     @Query("description_ar") description_ar: String?,
                     @Query("description_en") description_en:String?):Call<UserResponse>
    @Multipart
    @POST("edit-profile-provider")
    fun EditAvatar(@Query("lang") lang:String,
                     @Query("user_id") user_id:Int,
                     @Part avatar:MultipartBody.Part):Call<UserResponse>
    @POST("switch-notification")
    fun switch(@Query("user_id") user_id:Int,
               @Query("switch") switch:Int?):Call<SwitchNotification>
    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Query("user_id") user_id: Int,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>

    @POST("update-service-price")
    fun updatePrice(@Query("lang") lang:String,
                    @Query("provider_id") provider_id:Int,
                    @Query("prodect_id") prodect_id:Int,
                    @Query("have_services") have_services:Int,
                    @Query("price") price:String):Call<AboutAppResponse>

    @POST("unread-notifications")
    fun UnRead(@Query("user_id") user_id: Int):Call<SwitchNotification>
    @POST("delete-notification")
    fun delete(@Query("lang") lang: String,
               @Query("user_id") user_id:Int,
               @Query("notification_id") notification_id:Int):Call<AboutAppResponse>

    @POST("delete-product")
    fun deleteService(@Query("lang") lang:String,
                      @Query("provider_id") provider_id:Int,
                      @Query("prodect_id") prodect_id:Int):Call<AboutAppResponse>
}