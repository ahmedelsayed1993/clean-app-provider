package com.aait.cleanappprovider.Models

import java.io.Serializable

class InvoiceResponse:BaseResponse(),Serializable {
    var data:ArrayList<ItemInvoiceModel>?=null
    var delivery:String?=null
    var tax:String?=null
    var total_tax:String?=null
    var total:String?=null

}