package com.aait.cleanappprovider.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var address:String?=null
    var image:String?=null
}