package com.aait.cleanappprovider.Models

import java.io.Serializable

class Services : Serializable {
    var service_id:Int?=null
    var price:String?=null
    var subcategory_id:String?=null


    constructor(service_id: Int?, price: String?) {
        this.service_id = service_id
        this.price = price
    }

    constructor(service_id: Int?, price: String?, subcategory_id: String?) {
        this.service_id = service_id
        this.price = price
        this.subcategory_id = subcategory_id
    }



}