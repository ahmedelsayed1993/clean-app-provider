package com.aait.cleanappprovider.Models

import java.io.Serializable

class LocationServiceModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var price:String?=null
    var services_price:String?=null
    var image:String?=null
}