package com.aait.cleanappprovider.Models

import java.io.Serializable

class OrderDelegateResponse:BaseResponse(),Serializable {
    var data:ArrayList<OrderDelegateModel>?=null
}