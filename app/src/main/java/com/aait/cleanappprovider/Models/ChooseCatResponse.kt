package com.aait.cleanappprovider.Models

import java.io.Serializable

class ChooseCatResponse:BaseResponse(),Serializable {
    var active:String?=null
    var category_id:String?=null
    var category_name:String?=null
    var data:String?=null
    var category_key:String?=null
}