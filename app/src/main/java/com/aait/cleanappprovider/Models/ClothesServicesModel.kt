package com.aait.cleanappprovider.Models

import java.io.Serializable

class ClothesServicesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var price:String?=null
    var services_price:ArrayList<ClothesServiceModel>?=null
}