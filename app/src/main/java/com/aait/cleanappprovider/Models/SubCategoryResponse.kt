package com.aait.cleanappprovider.Models

import java.io.Serializable

class SubCategoryResponse:BaseResponse(),Serializable {
    var data:ArrayList<SubCategoryModel>?=null
    var services:ArrayList<ListModel>?=null
}