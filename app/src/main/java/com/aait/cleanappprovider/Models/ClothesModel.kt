package com.aait.cleanappprovider.Models

import java.io.Serializable

class ClothesModel:Serializable {
    var product:Int?= null
    var subcategory_id:String?=null

    var services:ArrayList<Services>?=null

    constructor(product: Int?,subcategory_id:String? , services: ArrayList<Services>?) {
        this.product = product
        this.subcategory_id = subcategory_id
        this.services = services
    }
}