package com.aait.cleanappprovider.Models

import java.io.Serializable

class ServicesCarModel:Serializable {
    var service_id:String?=null
    var price:String?=null
    var subcategory_id:String?=null


    constructor(service_id: String?, price: String?) {
        this.service_id = service_id
        this.price = price
    }

    constructor(service_id: String?, price: String?, subcategory_id: String?) {
        this.service_id = service_id
        this.price = price
        this.subcategory_id = subcategory_id
    }
}