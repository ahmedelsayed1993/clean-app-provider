package com.aait.cleanappprovider.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var id:Int?=null
    var username:String?= null
    var image:String?=null
    var lat:String?=null
    var lng:String?=null
    var category:String?=null
    var items:ArrayList<ItemModel>?=null
    var products:ArrayList<ProductsModel>?=null
    var provider_id:Int?=null
    var name:String?=null
    var delegate_id:Int?=null
}