package com.aait.cleanappprovider.Models

import java.io.Serializable

class ItemInvoiceModel:Serializable {
    var id:Int?= null
    var product:String?=null
    var services:String?=null
    var count:String?=null
    var price:Float?=null
}