package com.aait.cleanappprovider.Models

import java.io.Serializable

class SubCategoryModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var category_id:Int?=null
    var category_name:String?=null
    var selected:Boolean?=false
    var products:ArrayList<ProductModel>?=null
}