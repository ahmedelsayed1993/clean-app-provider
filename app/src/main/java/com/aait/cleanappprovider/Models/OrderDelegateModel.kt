package com.aait.cleanappprovider.Models

import java.io.Serializable

class OrderDelegateModel :Serializable{
    var id:Int?=null
    var order_id:Int?=null
    var provider_name:String?=null
    var address:String?=null
    var image:String?=null
}