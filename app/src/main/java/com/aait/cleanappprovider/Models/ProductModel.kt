package com.aait.cleanappprovider.Models

import java.io.Serializable

class ProductModel:Serializable {
    var id:Int?= null
    var name:String?=null
    var have_services:Int?=null
    var image:String?=null
    var services:ArrayList<ServiceModel>?=null
    var count:Int?=null
    var selected:Boolean = false
    var price:String?=null
}