package com.aait.cleanappprovider.Models

import java.io.Serializable

class CarServiceResponse:BaseResponse(),Serializable {
    var data:ArrayList<LocationServiceModel>?=null
    var subcategory_price:ArrayList<CarServiceModel>?=null
}