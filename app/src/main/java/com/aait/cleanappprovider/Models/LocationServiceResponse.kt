package com.aait.cleanappprovider.Models

import java.io.Serializable

class LocationServiceResponse:BaseResponse(),Serializable {
    var data:ArrayList<LocationServiceModel>?=null
}