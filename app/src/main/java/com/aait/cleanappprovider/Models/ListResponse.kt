package com.aait.cleanappprovider.Models

import java.io.Serializable

class ListResponse :BaseResponse(),Serializable {
    var data:ArrayList<ListModel>?=null
}