package com.aait.cleanappprovider.Models

import java.io.Serializable

class TrackOrderDelegateResponse:BaseResponse(),Serializable {
    var data:TrackOrderDelegateModel?=null
}