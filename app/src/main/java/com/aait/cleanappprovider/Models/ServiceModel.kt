package com.aait.cleanappprovider.Models

import java.io.Serializable

class ServiceModel:Serializable {
    var service_id:Int?=null
    var price:String?=null
    var name:String?=null

    constructor(service_id: Int?, price: String?) {
        this.service_id = service_id
        this.price = price
    }
}