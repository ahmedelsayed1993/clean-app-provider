package com.aait.cleanappprovider.Models

import java.io.Serializable

class ClothesServiceModel:Serializable {
    var id:Int?=null
    var price:String?=null
    var service_id:String?=null
    var service_name:String?=null

    constructor(price: String?, service_id: String?) {
        this.price = price
        this.service_id = service_id
    }
}