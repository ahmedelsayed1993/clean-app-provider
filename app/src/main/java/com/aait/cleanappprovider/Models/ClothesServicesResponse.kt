package com.aait.cleanappprovider.Models

import java.io.Serializable

class ClothesServicesResponse:BaseResponse(),Serializable {
    var data:ArrayList<ClothesServicesModel>?=null
}