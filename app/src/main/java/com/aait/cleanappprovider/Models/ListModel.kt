package com.aait.cleanappprovider.Models

import java.io.Serializable

class ListModel :Serializable{
    var id:Int?=null
    var name:String?=null
    var category_key:String?=null
    var checked:Int?= null
    var price:String?=null

    constructor(id: Int?, name: String?, category_key: String?) {
        this.id = id
        this.name = name
        this.category_key = category_key
    }

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }

}