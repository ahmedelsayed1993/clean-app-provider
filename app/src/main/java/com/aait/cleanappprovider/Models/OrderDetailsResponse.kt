package com.aait.cleanappprovider.Models

import java.io.Serializable

class OrderDetailsResponse:BaseResponse(),Serializable {
    var data:OrderDetailsModel?=null
    var check_time:Int?=null
}