package com.aait.cleanappprovider.Models

import java.io.Serializable

class DelegateModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var rate:Float?=null
    var active:Int?=null
    var confirm:Int?=null
}