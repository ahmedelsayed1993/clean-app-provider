package com.aait.cleanappprovider.Models

import java.io.Serializable

class SendOrderDelegateResponse:BaseResponse(),Serializable {
    var data:DelegateDetailsModel?=null
    var delegate_orders:ArrayList<DelegateOrdersModel>?=null
}