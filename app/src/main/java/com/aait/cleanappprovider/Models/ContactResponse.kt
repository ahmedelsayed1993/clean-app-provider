package com.aait.cleanappprovider.Models

import java.io.Serializable

class ContactResponse:BaseResponse(),Serializable {
    var data:ArrayList<SocialModel>?=null
}