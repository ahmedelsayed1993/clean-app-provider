package com.aait.cleanappprovider.Models

import java.io.Serializable

class CarModel:Serializable {
    var product:String?=null
    var price:String?=null
    var subcategory_id:String?=null

    constructor(product: String?, price: String?,subcategory_id:String?) {
        this.product = product
        this.price = price
        this.subcategory_id = subcategory_id
    }
}