package com.aait.cleanappprovider.Models

import java.io.Serializable

class DelegatesResponse:BaseResponse(),Serializable {
    var data:ArrayList<DelegateModel>?=null
}