package com.aait.cleanappprovider.Models

import java.io.Serializable

class DelegateDetailsModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var phone:String?=null
    var rate:Float?=null
    var password:String?=null
    var description:String?=null
}