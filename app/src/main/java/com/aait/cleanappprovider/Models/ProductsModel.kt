package com.aait.cleanappprovider.Models

import java.io.Serializable

class ProductsModel:Serializable {
    var id:Int?=null
    var product:String?=null
    var count:String?=null
}