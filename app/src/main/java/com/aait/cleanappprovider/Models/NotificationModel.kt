package com.aait.cleanappprovider.Models

import java.io.Serializable

class NotificationModel:Serializable {
    var id:Int?=null
    var content:String?=null
    var type:String?=null
    var seen:Int?=null
    var order_id:Int?=null
    var user_id:Int?=null
    var provider_id:Int?=null
    var delegate_id:Int?=null
    var status_order:String?=null
    var order_delegate_id:Int?=null
}