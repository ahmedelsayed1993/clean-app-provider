package com.aait.cleanappprovider.Models

import java.io.Serializable

class UserModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var phone:String?=null
    var code:String?=null
    var user_type:String?=null
    var status:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var provider_category:String?=null
    var category_name:String?=null
    var category_key:String?=null
    var lat:String?=null
    var lng:String?=null
    var address:String?=null
    var star_id:Int?=null
    var star_image:String?=null
    var description_ar:String?= null
    var description_en:String?=null
}