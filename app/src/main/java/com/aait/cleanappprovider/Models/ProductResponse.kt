package com.aait.cleanappprovider.Models

import java.io.Serializable

class ProductResponse:BaseResponse(),Serializable {
    var username:String?=null
    var data:ArrayList<ProductModel>?=null
    var services:MutableList<ServiceModel>?=null
}