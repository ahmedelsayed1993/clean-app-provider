package com.aait.cleanappprovider.Listeners

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
